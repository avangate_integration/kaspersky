'use strict';
import {argv}  from 'yargs';

/**
 * Global Config
 */
const Config = {
    defaults: {
        language: 'en',
    },
    browserPort: 3000,
    UIPort: 3001,
    paths: {
        scripts: {
            src: ['./app/js/**/*.js'],
            dest: './build/public/js/',
            test: './tests/**/*.js',
            gulp: './gulp/**/*.js',
            dependencies: ['./node_modules/bootstrap/dist/js/bootstrap.min.js',
                './node_modules/jquery/dist/jquery.min.js',
                './node_modules/tether/dist/js/tether.min.js']
        },
        images: {
            src: './app/images/**/*.{jpeg,jpg,png,gif}',
            dest: './build/public/images/'
        },
        styles: {
            src: ['./app/styles/**/*.scss'],
            dest: './build/public/css/'
        },
        locales: {
            src: './app/**/*.js',
            dest: './app/locales/'
        },
        translations: {
            src: './app/locales/**/*.po',
            dest: './build/public/translations/'
        },
        sourceDir: './app/',
        buildDir: './build/',
        publicDir: './build/public/',
        testFiles: './tests/**/*.{js,jsx}',
        nodeModulesDir: './node_modules/'
    },
    assetExtensions: [
        'js',
        'css',
        'map',
        'png',
        'jpe?g',
        'gif',
        'svg',
        'eot',
        'otf',
        'ttc',
        'ttf',
        'woff2?'
    ]
};

// Set env fallback.
let env = argv.env || 'dev';

/**
 * Set environments.
 */
Config.environments = {
    'dev': {
        vendorCode: 'KASPLB',
        repository: 'git@cvs.avangate.local:ps_dev',
        branch: 'kaspersky.ro',
        languages: ['en, es'],
        domain: 'http://localhost',
        ApiEndPoint: 'https://marketplace-api-dev.avangate.com/api'
    },
    'kaspersky.ro': {
        vendorCode: 'KASPLB',
        repository: 'git@gitlab.avangate.local:ps/kaspersky-marketplace-release.git',
        branch: 'kaspersky.ro',
        languages: ['en'],
        domain: 'http://kaspersky.ro'
    }
};

/**
 * Set API configuration.
 */
Config.API = {
    endPoint: (Config.environments.hasOwnProperty(env) && Config.environments[env].ApiEndPoint ? Config.environments[env].ApiEndPoint : 'https://marketplace-api-dev.avangate.com/api'),
    header: {
        'X-Avangate-Authentication': ( Config.environments.hasOwnProperty(env) ? Config.environments[env].vendorCode : 'KASPLB'), // Vendor Code
        'Accept': 'application/json',
    }
};

export default Config;