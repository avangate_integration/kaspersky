'use strict';

import fs          from 'fs';
import onlyScripts from './util/script-filter';

const gulpTasks = fs.readdirSync('./gulp/tasks/').filter(onlyScripts);

gulpTasks.forEach(function (task) {
    require('./tasks/' + task);
});