import gulp        from 'gulp';
import runSequence from 'run-sequence';
import Config  from '../../Config';
var reactGettextParser = require('react-gettext-parser').gulp;
var po2json = require('gulp-po2json');

gulp.task('locales', function () {
    return gulp.src(Config.paths.locales.src)
        .pipe(reactGettextParser({
            output: 'en.po',
            funcArgumentsMap: {
                gettext: ['msgid'],
                t: ['msgid'],
                tct: ['msgid'],
                dgettext: [null, 'msgid'],
                ngettext: ['msgid', 'msgid_plural'],
                tn: ['msgid', 'msgid_plural'],
                dngettext: [null, 'msgid', 'msgid_plural'],
                pgettext: ['msgctxt', 'msgid'],
                dpgettext: [null, 'msgctxt', 'msgid'],
                npgettext: ['msgctxt', 'msgid', 'msgid_plural'],
                dnpgettext: [null, 'msgid', 'msgid_plural'],
            }
        }))
        .pipe(gulp.dest(Config.paths.locales.dest));
});

gulp.task('locales2json', function () {
        return gulp.src([Config.paths.translations.src])
        .pipe(po2json({ format: 'jed' }))
        .pipe(gulp.dest(Config.paths.translations.dest));
});

gulp.task('translations', function (cb) {
    cb = cb || function () { };
    runSequence(['locales', 'locales2json'], cb);
});
