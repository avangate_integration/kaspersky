'use strict';

import gulp   from 'gulp';
import eslint from 'gulp-eslint';
import Config  from '../../Config';

gulp.task('lint', function() {
  return gulp.src([Config.paths.scripts.src, Config.paths.scripts.test, Config.paths.scripts.gulp])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});
