'use strict';

import gulp   from 'gulp';
import Config  from '../../Config';

gulp.task('copyDependencies', function() {
  return gulp.src(Config.paths.scripts.dependencies)
        .pipe(gulp.dest(Config.paths.scripts.dest));
});
