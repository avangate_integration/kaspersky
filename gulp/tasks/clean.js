'use strict';

import gulp   from 'gulp';
import del    from 'del';
import Config  from '../../Config';

gulp.task('clean', function() {
  return del([Config.paths.publicDir]);
});
