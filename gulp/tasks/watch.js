'use strict';

import gulp   from 'gulp';
import Config  from '../../Config';

gulp.task('watch', ['browserSync'], function () {
    // Scripts are automatically watched by Watchify inside Browserify task
    gulp.watch(Config.paths.styles.src, ['sass']);
    gulp.watch(Config.paths.translations.src, ['translations']);
    gulp.watch(Config.paths.images.src, ['imagemin']);
    gulp.watch(Config.paths.sourceDir + 'index.html', ['copyIndex']);
});
