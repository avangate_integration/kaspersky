'use strict';

import gulp   from 'gulp';
import Config  from '../../Config';

gulp.task('copyFonts', function () {
  return gulp.src([Config.paths.sourceDir + 'fonts/**/*', Config.paths.nodeModulesDir + 'font-awesome/fonts/*'])
    .pipe(gulp.dest(Config.paths.publicDir + 'fonts/'));
});
