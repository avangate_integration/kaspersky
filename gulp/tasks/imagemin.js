'use strict';

import gulp        from 'gulp';
import gulpif      from 'gulp-if';
import imagemin    from 'gulp-imagemin';
import browserSync from 'browser-sync';
import Config  from '../../Config';

gulp.task('imagemin', function() {
  return gulp.src(Config.paths.images.src)
    .pipe(gulpif(global.isProd, imagemin()))
    .pipe(gulp.dest(Config.paths.images.dest))
    .pipe(gulpif(browserSync.active, browserSync.reload({ stream: true, once: true })));
});
