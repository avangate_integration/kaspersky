'use strict';

import gulp         from 'gulp';
import git          from 'gulp-git';
import {argv}       from 'yargs';
import Config       from '../../Config';
import del          from 'del';
import runSequence  from 'run-sequence';
import mkdirp  from 'mkdirp';
 
gulp.task('deploy:git:clean', function (cb) {
    return del([Config.paths.buildDir + '/**'], cb);
});

gulp.task('deploy:git:createDirs', function (cb) {
    return mkdirp(Config.paths.buildDir, function (err) {
        if (err) console.error(err)
        cb();
    });
});

// gulp.task('deploy:git:init', function (cb) {
//     return git.init({ cwd: Config.paths.buildDir, quiet: true }, function (err) {
//         if (err) console.log(err);
//         cb();
//     });
// });

// gulp.task('deploy:git:addremote', function (cb) {
//     return git.addRemote('release', Config.environments[argv.env].repository, { cwd: Config.paths.buildDir, quiet: true }, function (err) {
//         if (err) console.log(err);
//         cb();
//     });
// });

// gulp.task('deploy:git:pull', function (cb) {
//     return git.pull('release', Config.environments[argv.env].branch, { args: '', cwd: Config.paths.buildDir, quiet: true }, function (err) {
//         if (err) console.log(err);
//         cb();
//     });
// });

// gulp.task('deploy:git:merge', function (cb) {
//     return git.merge(Config.environments[argv.env].branch, { args: ' --strategy recursive --strategy-option ours ', cwd: Config.paths.buildDir, quiet: true }, function (err) {
//         if (err) console.log(err);
//         cb();
//     });
// });

gulp.task('deploy:git:clone', function (cb) {
    return git.clone(Config.environments[argv.env].repository, { args: ' --branch ' + Config.environments[argv.env].branch + ' .', cwd: Config.paths.buildDir, quiet: true }, function (err) {
        if (err) console.log(err);
        cb();
    });
});

gulp.task('deploy:git:checkout', function (cb) {
    return git.checkout(Config.environments[argv.env].repository, { args: ' -b', cwd: Config.paths.buildDir, quiet: true }, function (err) {
        if (err) console.log(err);
        cb();
    });
});

gulp.task('deploy:git:add', function (cb) {
    return gulp.src(Config.paths.buildDir + '/')
        .pipe(git.add({ args: " . -f", cwd: Config.paths.buildDir, quiet: true }));
});

gulp.task('deploy:git:commit', function (cb) {
    let commitMsg = 'Gulp: Deploy to ' + argv.env;
    if (argv.m) {
        commitMsg = argv.m;
    } else {
        console.log('No "--m" commit message. Adding a standard one.');
    }
    return gulp.src(Config.paths.buildDir + '/*')
        .pipe(git.commit(commitMsg, { cwd: Config.paths.buildDir, quiet: true }));

});

gulp.task('deploy:git:push', function (cb) {
    return git.push('origin', Config.environments[argv.env].branch, { args: "", cwd: Config.paths.buildDir, quiet: true }, function (err) {
        if (err) console.log(err);
        cb();
    });

});

gulp.task('deploy', function (cb) {
    if (!(argv && argv.env && Config.environments.hasOwnProperty(argv.env))) {
        console.log('Environment "--env" NOT passed or not in the config. Exiting...');
        return;
    }
    runSequence(
        'deploy:git:clean',
        'deploy:git:createDirs',
        'deploy:git:clone', 
        'prod', 
        'deploy:git:add',
        'deploy:git:commit', 
        'deploy:git:push');
});
