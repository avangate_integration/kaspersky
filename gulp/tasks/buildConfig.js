'use strict';

import gulp   from 'gulp';
import jsonfile  from 'jsonfile';
import fs   from 'fs';
import Config  from '../../Config';
import {argv}  from 'yargs';

function traverseObjectAndNull(o) {
    for (let i in o) {
        if (!!o[i] && typeof (o[i]) == "object") {
            traverseObjectAndNull(o[i]);
        } else o[i] = null;
    }
}

let buildConfig, buildConfigSample = {};
// Set env fallback.
let env = argv.env || 'dev';
let lang = (function(env){
    try {
        return Config.environments[env].languages[0];
    } catch(err) {
        return Config.defaults.language;
    }
})(env);

buildConfig = {
    API: Config.API,
    language: lang,
    domain: Config.environments[env].domain,
};

// buildConfigSample = JSON.parse(JSON.stringify(buildConfig));

// traverseObjectAndNull(buildConfigSample);

// let gitIgnoreSrc = '/config.js';

gulp.task('buildConfig', function () {
    fs.writeFile(Config.paths.publicDir + '/config.js', 'var MarketplaceConfig=' + JSON.stringify(buildConfig), function (err) {
        if (err) return console.log(err);
    })

    // fs.writeFile(Config.paths.buildDir + '/config.sample.js', 'var MarketplaceConfig=' + JSON.stringify(buildConfigSample), function (err) {
    //     if (err) return console.log(err);
    // });
    // fs.writeFile(Config.paths.buildDir + '/.gitignore', gitIgnoreSrc, function (err) {
    //     if (err) return console.log(err);
    // });
     
});