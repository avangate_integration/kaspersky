'use strict';

import gulp   from 'gulp';
import Config  from '../../Config';

gulp.task('copyIndex', function() {
  gulp.src([Config.paths.sourceDir + 'index.html', Config.paths.sourceDir + '.htaccess']).pipe(gulp.dest(Config.paths.publicDir));
});
