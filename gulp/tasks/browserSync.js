'use strict';

import url         from 'url';
import browserSync from 'browser-sync';
import gulp        from 'gulp';
import Config       from '../../Config';

gulp.task('browserSync', function() {
  const DEFAULT_FILE = 'index.html';
  const ASSET_EXTENSION_REGEX = new RegExp(`\\b(?!\\?)\\.(${Config.assetExtensions.join('|')})\\b(?!\\.)`, 'i');

  browserSync.init({
    server: {
      baseDir: Config.paths.publicDir,
      middleware: function(req, res, next) {
        const fileHref = url.parse(req.url).href;

        if ( !ASSET_EXTENSION_REGEX.test(fileHref)) {
          req.url = '/' + DEFAULT_FILE;
        }

        return next();
      }
    },
    port: Config.browserPort,
    ui: {
      port: Config.UIPort
    },
    ghostMode: {
      links: false
    }
  });
});
