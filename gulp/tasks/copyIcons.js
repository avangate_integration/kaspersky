'use strict';

import gulp   from 'gulp';
import Config  from '../../Config';

gulp.task('copyIcons', function() {
  return gulp.src(['./*.png', './favicon.ico'])
    .pipe(gulp.dest(Config.paths.publicDir));
});
