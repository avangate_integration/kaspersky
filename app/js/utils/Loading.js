/* eslint-disable prefer-template, quote-props */
import React from 'react';
import Loader from 'react-loader-advanced';
import Spinner from 'react-loading';


class Loading extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {
            show,
            delay,
            priority,
            background,
            spinnerType,
            spinnerColor,
            spinnerWidth,
            spinnerHeight,
            spinnerClass,
            children
        } = this.props;
        return (
            <Loader show={show} priority={priority} message={<div className={spinnerClass}><Spinner type={spinnerType} color={spinnerColor} width={spinnerWidth}  height={spinnerHeight} /></div>} backgroundStyle={{ backgroundColor: background }}>
                {children}
            </Loader>
        );
    }
}

Loading.propTypes = {
    show: React.PropTypes.bool,
    delay: React.PropTypes.number,
    priority: React.PropTypes.number,
    background: React.PropTypes.string,
    spinnerType: React.PropTypes.string,
    spinnerColor: React.PropTypes.string,
    spinnerWidth: React.PropTypes.string,
    spinnerHeight: React.PropTypes.string,
    spinnerClass: React.PropTypes.string,
    children: React.PropTypes.node
};

/**
 * For docs check:
 * https://www.npmjs.com/package/react-loader-advanced
 * https://www.npmjs.com/package/react-loading
 */
Loading.defaultProps = {
    show: true,
    priority: 0,
    delay: 1000,
    background: '#fff',
    spinnerType: 'cylon',  /* 
    blank
    balls
    bars
    bubbles
    cubes
    cylon
    spin
    spinningBubbles
    spokes
    */
    spinnerColor: '#009a82',
    spinnerWidth: '64',
    spinnerHeight: '64',
    spinnerClass: 'd-inline-block m-x-auto text-xs-center',
};

export default Loading;