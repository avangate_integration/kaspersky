'use strict';

import _ from 'lodash';
var params = require('query-params');

const Helpers = {
    /**
     * 
     * 
     * @param {object} obj
     * @returns
     */
    objectSize(obj) {
        var objsize = 0;
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                objsize++;
            }
        }
        return objsize;
    },
    
    /**
     * 
     * 
     * @param {object} obj
     */
    getQueryString(obj) {
        if (typeof obj === 'object' && this.objectSize(obj)) {
            return ('?' + params.encode(obj));
        } else {
            return '';
        }
    },

    /**
     * 
     * 
     * @param {object} obj
     * @param {boolean} convert
     * @returns
     */
    processObjectKeys(obj, convert) {
        let output;

        if (_.isDate(obj) || _.isRegExp(obj) || !_.isObject(obj)) {
            return obj;
        } else if (_.isArray(obj)) {
            output = _.map(obj, item => {
                return this.processObjectKeys(item, convert);
            });
        } else {
            output = {};
            _.forOwn(obj, (value, key) => {
                output[convert(key)] = this.processObjectKeys(obj[key], convert);
            });
        }

        return output;
    }

};

export default Helpers;
