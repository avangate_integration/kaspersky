'use strict';

import APIUtils from './APIUtils';
import helpers from './Helpers';


const AvaAPI = {

    /**
     * Get Products
     * 
     * @param {object} params
     * @returns
     */
    getProducts(params) {
        return APIUtils.get('/products' + helpers.getQueryString(params));
    }
};

export default AvaAPI;
