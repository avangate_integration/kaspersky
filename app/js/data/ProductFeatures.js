'use strict';

import React, { PropTypes } from 'react';
import {t, tct} from '../Locales';

const ProductFeatures = [
    {
        feature: 'security',
        title: t('Security'),
        description: t('Rigorous protection technologies')
    },
    {
        feature: 'performance',
        title: t('Performance'),
        description: t('Optimized for efficiency')
    },
    {
        feature: 'simplicity',
        title: t('Simplicity'),
        description: t('Easy-to-manage security')
    },
    {
        feature: 'privacy',
        title: t('Privacy'),
        description: t('Identify protection & more')
    },
    {
        feature: 'money',
        title: t('Money'),
        description: t('Security for online banking & shopping')
    },
    {
        feature: 'kids',
        title: t('Kids'),
        description: t('Protection against online risks')
    },
    {
        feature: 'pc_mac_mobile',
        title: t('PC, MAC & Mobile'),
        description: t('Security for multiple devices')
    },
    {
        feature: 'password',
        title: t('Password'),
        description: t('Secure storage & synchronization')
    },
    {
        feature: 'files',
        title: t('Files'),
        description: t('Protection for photos, music & more')
    }
]

export default ProductFeatures;