'use strict';

import React, { PropTypes } from 'react';
import {t, tct} from '../Locales';

const HomepageProducts = [
    {
        code: '7CBC44F3B4',
        recommended: false,
        promo: null,
        multi_device: false,
        features: {
            security: true,
            performance: true,
            simplicity: true,
            privacy: true,
            money: true,
            kids: false,
            pc_mac_mobile: false,
            password: false,
            files: false
        }
    },
    {
        code: '4605281',
        recommended: true,
        promo: null,
        multi_device: true,
        features: {
            security: true,
            performance: true,
            simplicity: true,
            privacy: true,
            money: true,
            kids: true,
            pc_mac_mobile: false,
            password: false,
            files: false
        }
    },
    {
        code: '214865B32E',
        recommended: false,
        promo: tct('Now 10 GB[br]for FREE', { br: <br /> }),
        multi_device: true,
        features: {
            security: true,
            performance: true,
            simplicity: true,
            privacy: true,
            money: true,
            kids: true,
            pc_mac_mobile: true,
            password: true,
            files: true
        }
    }
];

export default HomepageProducts;