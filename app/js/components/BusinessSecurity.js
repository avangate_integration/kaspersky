import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class BusinessSecurity extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {

        return (
            <section id="security" className="security p-a-3">
              <Container>
                  <Row>
                      <Col>
                          <h2 className="text-xs-center component-title-white">{t('Security for business')}</h2>
                      </Col>
                  </Row>
                  <Row>
                    <Col xs="5">
                        <h4 className="text-xs-center title-white">{t('Very small business')}</h4>
                        <img src="/images/home_page/small-office-security.png" className="img-fluid security-prod" />
                        <Button color="secondary" className="text-uppercase btn security-btn">{t('Learn more')}</Button>
                    </Col>
                    <Col xs="2">
                      <img src="/images/home_page/ruler.png" className="img-fluid ruler" />
                    </Col>
                    <Col xs="5">
                      <h4 className="text-xs-center title-white">{t('B2B')}</h4>
                      <img src="/images/home_page/total-security.png" className="img-fluid security-prod" />
                      <Button color="secondary" className="text-uppercase btn security-btn">{t('Learn more')}</Button>
                    </Col>
                  </Row>
              </Container>
            </section>
        );
    }

}

export default BusinessSecurity;
