'use strict';

import React from 'react';

class SocialLinks extends React.Component {

    constructor(props) {
        super(props);
    }

  render() {
    return (
        <ul className="social-links row list-unstyled">
            <li className="social-link col-xs-2 offset-xs-1">
                <a href="#">
                <i className="fa fa-facebook" aria-hidden="true"></i>
                <span className="sr-only">Facebook</span>
                </a>
            </li>
            <li className="social-link col-xs-2">
                <a href="#">
                <i className="fa fa-twitter" aria-hidden="true"></i>
                <span className="sr-only">Twitter</span>
                </a>
            </li>
            <li className="social-link col-xs-2">
                <a href="#">
                <i className="fa fa-linkedin" aria-hidden="true"></i>
                <span className="sr-only">LinkedIn</span>
                </a>
            </li>
            <li className="social-link col-xs-2">
                <a href="#">
                <i className="fa fa-instagram" aria-hidden="true"></i>
                <span className="sr-only">Instagram</span>
                </a>
            </li>
            <li className="social-link col-xs-2">
                <a href="#">
                <i className="fa fa-google-plus" aria-hidden="true"></i>
                <span className="sr-only">Google Plus</span>
                </a>
            </li>
        </ul>
    );
  }
}

export default SocialLinks;