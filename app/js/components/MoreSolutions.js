import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t,tct} from '../Locales';

class MoreSolutions extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="more-solutions" className="more-solutions p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('More solutions for you and your family')}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="1" md="2" xs="12" className="m-t-2 total-security-wrapper">
                            <img src="/images/safe_kids_page/total-security.png"/>
                        </Col>
                        <Col lg="5" md="4" xs="12" className="total-security-content">
                            <p className="kaspersky">{t('Kaspersky')}</p>
                            <p className="subtitle m-t-1">{tct('Total [br] Security',{br: <br />})}</p>
                            <p>{t('multi-device')}</p>
                            <p>{t('Our ultimate protection for your family')}</p>
                        </Col>
                        <Col lg="1" md="2" xs="12" className="m-t-2 home-wrapper">
                            <img src="/images/safe_kids_page/home.png"/>
                        </Col>
                        <Col lg="5" md="4" xs="12" className="m-t-1 home-content">
                            <p className="subtitle m-t-2">{tct('Other [br] Home Products', {br: <br />})}</p>
                            <p className="m-t-3">{t('Choose which solution right for you')}</p>
                        </Col>
                    </Row>
                 </Container>
            </section>

        );
    }

}

export default MoreSolutions;
