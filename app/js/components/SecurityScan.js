import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class SecurityScan extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="scan" className="scan p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Kaspersky security scan for your pc for free')}</h2>
                        </Col>
                    </Row>
                    <Row>
                      <Col xs="12">
                        <img src="/images/proven_leader_page/bug-scan.png" className="img-fluid bugImg" />
                        <p className="text-xs-center kasgreen">{t('Kaspersky')}</p>
                        <h4 className="text-xs-center">{t('Security Scan')}</h4>
                        <p className="text-xs-center">{t('This free, easy-to-use scanner finds malaware that may be hidden on your PC - even if you already have an antivirus product installed. Get a detailed security status report - plus tips on improving PC protection.')}</p>
                        <Button color="secondary" className="text-uppercase">{t('Try now')}</Button>
                      </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default SecurityScan;
