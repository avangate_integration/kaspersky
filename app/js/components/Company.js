import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class Company extends React.Component {

    constructor(props) {
        super(props);
    }
    /**
     * the links from our website should opne in the same tab
     * otherwise, they should open in a blank one
     */
    handleClick(link) {
        window.open(link, '_self');
    }
    handleExternalClick(link) {
        window.open(link, '_blank');
    }
    render() {
        return (
            <section id="company" className="company p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Company') }</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="3" md="0"></Col>
                        <Col lg="6" md="12">
                            <p className="text-xs-center m-x-auto d-inline-block m-b-3">{t('We\'re one of the world\'s fastest growing IT security companies - and we\'re achieving growthin all of the regions we operate within.') }</p>
                        </Col>
                        <Col lg="3" md="0"></Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="4">
                            <img src="/images/about_us_page/about_us_1.png" className="img-fluid img-full-stretch"/>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Contact us') }</h6>
                            <p className="text-xs-center m-b-2">{t('Our teams are ready to help.') }</p>
                            <p className="text-xs-center">
                                <Button color="secondary" className="text-uppercase btn-company" onClick={() => this.handleClick('http://localhost:3000/contact-us') }>{t('Contact Kaspersky Lab') }</Button>
                            </p>
                        </Col>
                        <Col xs="12" md="4">
                            <img src="/images/about_us_page/about_us_2.png" className="img-fluid img-full-stretch"/>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Careers') }</h6>
                            <p className="text-xs-center m-b-2">{t('Want to join us?') }</p>
                            <p className="text-xs-center">
                                <Button color="secondary" className="text-uppercase btn-company" onClick={() => this.handleExternalClick('http://www.kaspersky.com/about/career') }>{t('Find out more') }</Button>
                            </p>
                        </Col>
                        <Col xs="12" md="4">
                            <img src="/images/about_us_page/about_us_3.png" className="img-fluid img-full-stretch"/>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Press center') }</h6>
                            <p className="text-xs-center m-b-2">{t('Need media contacts or information?') }</p>
                            <p className="text-xs-center">
                                <Button color="secondary" className="text-uppercase btn-company" onClick={() => this.handleExternalClick('https://press.kaspersky.com/') }>{t('Find out more') }</Button>
                            </p>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default Company;
