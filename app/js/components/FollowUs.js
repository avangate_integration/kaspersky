import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class FollowUs extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="follow-us" className="follow-us p-a-3">
              <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Follow us')}</h2>
                        </Col>
                    </Row>
                    <Row className="follow-background">
                      <Col xs="12" md="3" lg="2" className="offset-lg-1 followTxt">
                        <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Kaspersky Lab')}</h6>
                        <p className="text-xs-center">{t('Blog')}</p>
                      </Col>
                      <Col xs="12" md="3" lg="2" className="active followTxt">
                        <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Eugene Kaspersky')}</h6>
                        <p className="text-xs-center">{t('President and CEO')}</p>
                      </Col>
                      <Col xs="12" md="3" lg="2" className="followTxt">
                        <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Securelist')}</h6>
                        <p className="text-xs-center">{t('Analysis of Threats')}</p>
                      </Col>
                      <Col xs="12" md="3" lg="2" className="followTxt">
                        <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Threat post')}</h6>
                        <p className="text-xs-center">{t('News and Security')}</p>
                      </Col>
                    </Row>
                    <Row>
                     <Col xs="12" md="2" lg="2" className="offset-md-1 offset-lg-1 options">
                     
                      <Button color="secondary" className="btn linkedinBtn">{t('Connect on LinkedIn')}</Button>
                     </Col>
                     <Col xs="12" md="2" lg="2" className="options">
                      <Button color="secondary" className="btn fbBtn">{t('Follow on Facebook')}</Button>
                     </Col>
                     <Col xs="12" md="2" lg="2" className="options">
                      <Button color="secondary" className="btn gplusBtn">{t('Follow on Google+')}</Button>
                     </Col>
                     <Col xs="12" md="2" lg="2" className="options">
                      <Button color="secondary" className="btn twitBtn">{t('Follow on Twitter')}</Button>
                     </Col>
                     <Col xs="12" md="2" lg="2" className="options">
                      <Button color="secondary" className="btn utuBtn">{t('Subscribe on Youtube')}</Button>
                     </Col>
                    </Row>
               </Container>     
            </section>
        );
    }

}

export default FollowUs;
