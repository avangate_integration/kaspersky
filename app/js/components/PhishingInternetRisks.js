import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class PhishingInternetRisks extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="phishing-internet-risks" className="phishing-internet-risks p-a-3">
                <Container>
                    <Row className="m-t-2">
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Secures businesses against phishing & internet risks')}</h2>
                        </Col>
                    </Row>
                    <Row className="m-t-2 phishing-internet-risks-infos">
                        <Col lg="6" md="12">
                            <p className="m-b-2">{t('Although online financial transactions offer convenience benefits, they can also give cybercriminals an opportunity to steal money and banking details. Kaspersky Small Office Security helps keep your money, credit card numbers and bank account information safe:')}</p>
                            <p>
                              <ul className="ul-bullet">
                                <li>{t('Encrypts data on PC & file servers - to protect your data')}</li>
                                <li>{t('Backs up data - plus online backup - for data recovery')}</li>
                                <li>{t('Manages, securely stores & synchronizes all your passwords')}</li>
                              </ul>
                            </p>
                            <p className="m-t-3 btn-phishing-internet-risks-wrapper">
                                <Button color="secondary" className="text-uppercase">{t('Read more')}</Button>
                            </p>
                        </Col>
                        <Col lg="6" md="12" className="phishing-internet-risks-img">
                            <img src="/images/office_security_page/add-extra-security.png" className="img-fluid img-full-stretch"/>
                        </Col>
                    </Row>
                 </Container>
            </section>
        );
    }

}

export default PhishingInternetRisks;
