'use strict';

import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import LanguageSelector from './LanguageSelector';
import SocialLinks from './SocialLinks';
import {t} from '../Locales';

class Footer extends React.Component {

    constructor(props) {
        super(props);
    }

  render() {
    return (
      <footer id="footer">
        <Container className="p-y-3">
          <Row>
            <Col md="8">
              <section className="copyright-section pull-md-left">
                <p className="copyright">{t('Copyright')} 1997-{(new Date).getFullYear()} {t('Kaspersky Lab, lorem ipsum dolor sit amet elit adipi')}</p>
                <ul className="footer-links list-inline">
                  <li className="footer-link list-inline-item">
                    <a href="#">{t('Contact Us')}</a>
                  </li>
                  <li className="footer-link list-inline-item">
                    <a href="#">{t('About Us')}</a>
                  </li>
                  <li className="footer-link list-inline-item">
                    <a href="#">{t('Partners')}</a>
                  </li>
                </ul>
              </section>
            </Col>
            <Col md="4">
              <section className="social-section pull-xs-left pull-md-right">
                <SocialLinks />
                <LanguageSelector />
              </section>
            </Col>
          </Row>
        </Container>
      </footer>

    );
  }
}

export default Footer;