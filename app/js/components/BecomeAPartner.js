import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';

class BecomeAPartner extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="become-a-partner" className="banner lisible-text-banner become-a-partner p-a-3">
                <Container>
                    <Row>
                        <Col lg={{ size: 10, offset: 1 }} className="text-xs-center">
                            <h2 className="text-xs-center component-title m-b-2">{t('Become a partner')}</h2>
                            <div className="lisible-text">
                                <p className="text-xs-center m-x-auto d-inline-block safe-subtitle">{tct('Partners receive extensive support - ranging from training, to technical support, to marketing materials.[br]Kaspersky Lab works closely with each partner - to help ensure business success.', {
                                    br: <br />
                                })}</p>
                            </div>
                            <div className="text-xs-center m-t-1">
                                <Button color="secondary" className="text-uppercase btn-green btn-become-a-partner">{t('Try Now')}</Button>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default BecomeAPartner;
