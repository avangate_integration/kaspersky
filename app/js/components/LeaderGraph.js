import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';

class LeaderGraph extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="leader-graph" className="p-a-3">
              <Container className="leader-graph">
                <Row>
                  <Col xs="12">
                    <h1 className="text-xs-center leaderh1">{t('Kaspersky Lab IS #1')}</h1>
                    <h3 className="text-xs-center leaderh3">{t('In 60 comparative tests')}</h3>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <h2 className="text-xs-center component-title">{t('No other security company even comes close')}</h2>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <p className="text-xs-center">{tct('In 94 comparative tests and reviews last year, Kaspersky products earned 60 first-place finishes [br] and 77 top-three finishes.', {
                          br: <br />
                      })}</p>
                    <img src="/images/proven_leader_page/proven-leader-graph.png" className="img-fluid graph"/>
                  </Col>
                </Row>
               </Container>     
            </section>
        );
    }

}

export default LeaderGraph;
