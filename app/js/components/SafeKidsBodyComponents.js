import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class MonitorsCommunications extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="safe-kids-body-components" className="safe-kids-body-components">
                <Container className="monitors-communications p-a-3">
                    <Row>
                        <Col lg="6" xs="12">
                            <h2 className="monitors-communications-title m-b-3 m-t-3">{t('Monitors communications')}</h2>
                            <p className="monitors-communications-description">{t('Lets you monitor kids\' communications...including Facebook activity - plus call & SMS messages on Android devices.')}</p>
                        </Col>
                        <Col lg="6" xs="12">
                            <img src="/images/safe_kids_page/monitors-communications.png" className="img-fluid monitors-communications-img"/> 
                        </Col>
                    </Row>
                </Container>
                <Container className="guards-against-online-risks p-a-3">
                    <Row>
                        <Col lg="6" xs="12">
                            <img src="/images/safe_kids_page/guards-against-online-risks.png" className="img-fluid guards-against-online-risks-img"/> 
                        </Col>
                        <Col lg="6" xs="12">
                            <h2 className="guards-against-online-risks-title m-b-3 m-t-3">{t('Guards against online risks')}</h2>
                            <p className="guards-against-online-risks-description">{t('Helps you guide your kids...so they can access types of website, content & apps you specify are appropiate for their age.')}</p>
                        </Col>
                    </Row>
                </Container>  
                 <Container className="confirms-location p-a-3">
                    <Row>
                        <Col lg="6" xs="12">
                            <h2 className="confirms-location-title m-b-3 m-t-3">{t('Confirm their location')}</h2>
                            <p className="confirms-location-description">{t('Shows your child\'s location on real-time map - and sends you alerts if they leave the safe area you\'ve defined.')}</p>
                        </Col>
                        <Col lg="6" xs="12">
                            <img src="/images/safe_kids_page/confirms-location.png" className="img-fluid confirms-location-img"/> 
                        </Col>
                    </Row>
                </Container>  
                 <Container className="manages-device-time p-a-3">
                    <Row>
                        <Col lg="6" xs="12">
                            <img src="/images/safe_kids_page/manages-device-time.png" className="img-fluid manages-device-time-img"/> 
                        </Col>
                        <Col lg="6" xs="12">
                            <h2 className="manages-device-time-title m-b-3 m-t-3">{t('Manages \'Device Time\'')}</h2>
                            <p className="manages-device-time-description">{t('Helps you mentor your kids on their use of the Web & devices - so it\'s easier to teach them how to manage their time.')}</p>
                        </Col>
                    </Row>
                </Container>                          
            </section>
        );
    }

}

export default MonitorsCommunications;
