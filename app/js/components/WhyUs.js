import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class WhyUs extends React.Component {

  constructor(props) {
    super(props);
  }
    handleClick(link) {
    window.open(link, '_self');
  }
    render() {
        return (
            <section id="why-us" className="why-us p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Why us')}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="6">
                            <img src="/images/home_page/graph.png" className="img-fluid graph"/>
                        </Col>
                        <Col xs="6">
                            <p className="m-x-auto d-inline-block m-b-3">{t('Rely on our award-winning products- to protect what\'s precious to you.')}<br/>
                            <strong>{t('KASPERSKY LAB IS #1 IN 60 COMPARATIVE TESTS.')}</strong></p>
                            <p className="m-x-auto d-inline-block m-b-3">{t('In 94 comparative tests and reviews last year, Kaspersky products earned 60 first-place finishes and 77 top-three finishes.')}</p>
                            <Button color="secondary" className="text-uppercase btn whyus-btn" onClick= {() => this.handleClick('http://localhost:3000/proven-leaders')}>{t('More')}</Button>
                            <img src="/images/home_page/award.png" className="img-fluid" />
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default WhyUs;
