import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class ManagementTeam extends React.Component {

    showMembers(e) {
        $(e.target).hide();
        $('.management-team-more').slideDown();
    }

  constructor(props) {
    super(props);
  }

    render() {
        
        return (
            <section id="management-team" className="management-team p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Management team')}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="3" md="0"></Col>
                        <Col lg="6" md="12">
                            <p className="text-xs-center m-x-auto d-inline-block m-b-3">{t('We\'re one of the world\'s fastest growing IT security companies - and we\'re achieving growthin all of the regions we operate within.')}</p>
                        </Col>
                        <Col lg="3" md="0"></Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="4">
                            <p className="text-xs-center">
                                <img src="/images/about_us_page/about_us_mgt-1.png" className="img-fluid img-full-stretch"/>
                            </p>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Eugene Kaspersky')}</h6>
                            <p className="text-xs-center m-b-2">{t('Chief Executive Officer and Chairman')}</p>

                        </Col>
                        <Col xs="12" md="4">
                            <p className="text-xs-center">
                                <img src="/images/about_us_page/about_us_mgt-2.png" className="img-fluid img-full-stretch"/>
                            </p>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Garry Kondakov')}</h6>
                            <p className="text-xs-center m-b-2">{t('Chief Business Officer')}</p>

                        </Col>
                        <Col xs="12" md="4">
                            <p className="text-xs-center">
                                <img src="/images/about_us_page/about_us_mgt-3.png" className="img-fluid img-full-stretch"/>
                            </p>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Andrey Tikhonov')}</h6>
                            <p className="text-xs-center m-b-2 andrey">{t('Chief Operating Officer')}</p>
                        </Col>
                    </Row>
                    <Row className="management-team-more">
                        <Col xs="12" md="4">
                            <p className="text-xs-center">
                                <img src="/images/about_us_page/about_us_mgt-4.png" className="img-fluid img-full-stretch"/>
                            </p>
                            <h6 className="text-xs-center text-uppercase font-weight-bold text-success m-y-2">{t('Nikita Shvetsov')}</h6>
                            <p className="text-xs-center m-b-2">{t('Acting Chief Technology Officer')}</p>
                        </Col>
                        <Col xs="12" md="4"> </Col>
                        <Col xs="12" md="4"></Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="4"></Col>
                        <Col xs="12" md="4">
                            <p className="text-xs-center">
                                <Button color="secondary" className="text-uppercase btn-management-team" onClick={this.showMembers}>{t('Show more')}</Button>
                            </p>
                        </Col>
                        <Col xs="12" md="4"></Col>
                    </Row>
                </Container>
            </section>

        );
    }

}

export default ManagementTeam;
