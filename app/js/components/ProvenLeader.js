import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';

class ProvenLeader extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="proven-leader" className="p-a-3">
              <Container className="proven-leader">
                    <Row>
                        <Col xs="12">
                           <h1 className="title-white">{tct('Kaspersky Lab [br] Proven leader', {
                                br: <br />
                            })}</h1>
                           <h6>{tct('Our award-winning technology [br] gives you the power to protect what matters [br] most to you Online.', {
                                br: <br />
                            })}</h6>
                        </Col>
                    </Row>
               </Container>     
            </section>
        );
    }

}

export default ProvenLeader;
