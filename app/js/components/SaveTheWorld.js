import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t,tct} from '../Locales';

class SaveTheWorld extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="save-the-world" className="save-the-world p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <nav className="breadcrumb">
                                <a className="m-r-1 active">{t('Home')}</a>
                                <a>{t('About Us')}</a>
                            </nav>
                        </Col>
                    </Row>
                    <Row className="m-t-2">
                        <Col lg="8" md="12">
                            <h1 className="title-save-the-world m-b-2">{tct('We are here [br] to save the world', {
                                br: <br />
                            })}</h1>
                            <p className="content-save-the-world">{tct('Kaspersky Lab is the world\'s largest, [br] privately held IT security company', {
                                br: <br />
                            })}</p>
                        </Col>
                        <Col lg="4" md="12"></Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default SaveTheWorld;
