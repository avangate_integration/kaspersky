import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class AboutUs extends React.Component {

    constructor(props) {
        super(props);
    }
    handleClick(link) {
        window.open(link, '_self');
    }
    render() {
        return (
            <section id="about-us" className="about-us p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('About us') }</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="7" className="about-us-infos">
                            <h2 className="font-italic m-b-2">{t('"We believe that everyone has the right to be free from cybersecurity fears."') }</h2>
                            <div className="about-us-member">{t('- Eugene Kaspersky') }</div>
                            <div className="about-us-function">{t('Chairman and CEO of Kaspersky Lab') }</div>
                            <p className="m-t-3 about-us-wrapper">
                                <Button className="btn-secondary text-uppercase btn-about-us" onclick={() => this.handleClick('http://localhost:3000/free-products') }>{t('Read more') }</Button>
                            </p>
                        </Col>
                        <Col lg="5"></Col>

                    </Row>
                </Container>
            </section>
        );
    }

}

export default AboutUs;
