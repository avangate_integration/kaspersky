'use strict';

import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import NavLinks from './NavLinks';
import {t} from '../Locales';

class Header extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header id="header" className="site-header">
                <div className="container">
                    <Navbar className="navbar" color="dark" light>
                        <NavbarBrand href="/"><img src="images/kaspersky-logo.jpg" /></NavbarBrand>
                        <button className="navbar-toggler pull-xs-right" type="button" data-toggle="collapse" data-target="#topmenu-collapsed-wraper" aria-controls="topmenu-collapsed-wraper" aria-expanded="false" aria-label="Toggle navigation"></button>
                        <NavLinks collapsed={false} />
                        <div className="collapse clearfix" id="topmenu-collapsed-wraper">
                            <NavLinks collapsed={true} />
                        </div>
                    </Navbar>
                </div>
            </header>
        );
    }

}

export default Header;