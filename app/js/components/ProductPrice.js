'use strict';

import React from 'react';
import {t, tct} from '../Locales';

/**
 * 
 * 
 * @class ProductPrice
 * @extends {React.Component}
 */
class ProductPrice extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let discount = Math.round((this.props.productData.price.gross - this.props.productData.price.grossDiscounted) * 100 / this.props.productData.price.gross);
        return (
            <div className={'product-price ' + this.props.className}>
                <div className="">{t('from')} <span className="price-new">{this.props.productData.price.grossDiscounted} {this.props.productData.price.currency}</span></div>
                <span className="price-old">{this.props.productData.price.gross} {this.props.productData.price.currency}</span>
                <div className="price-discount-wrapper">{t('save')} <span className="price-discount">{discount}</span>%</div>
            </div>
        );
    }
}

export default ProductPrice;