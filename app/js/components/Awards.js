import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class Awards extends React.Component {

    constructor(props) {
        super(props);
    }
    handleClick(link) {
        window.open(link);
    }
    render() {

        return (
            <section id="awards" className="awards p-a-3">
                <Container>
                    <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div className="carousel-inner" role="listbox">
                            <div className="carousel-item active">
                                <Row>
                                    <Col>
                                        <h2 className="text-xs-center component-title">{t('Awards') }</h2>
                                    </Col>
                                </Row>
                                <Row className="content">
                                    <Col lg="3" md="0"></Col>
                                    <Col lg="6" md="12">
                                        <p className="text-xs-center m-x-auto d-inline-block m-l-2 subtitle idc-subtitle">{t('Kaspersky Lab ranked as \'Leader\' by IDC') }</p>
                                        <p className="text-xs-center m-x-auto d-inline-block m-b-1">{t('Kaspersky Lab recognised as a \'Leader\' in the IDC MarketScape 2012 Western European study of Enterprise Endpoint Security companies.') }</p>
                                    </Col>
                                    <Col lg="3" md="0"></Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="4"></Col>
                                    <Col xs="12" md="4">
                                        <div className="text-xs-center idc-img-wrapper">
                                            <img src="/images/about_us_page/IDC-large-new.png" className="img-fluid idc-img"/>
                                        </div>
                                        <p className="text-xs-center">
                                            <Button color="secondary" className="text-uppercase btn-awards" onClick={() => this.handleClick('http://media.kaspersky.com/en/business-security/IDC_MarketScape_Western_European_Enterprise_Endpoint_Security_2012_Vendor_Analysis.pdf') }>{t('Visit') }</Button>
                                        </p>
                                    </Col>
                                    <Col xs="12" md="4"></Col>
                                </Row>
                            </div>
                            <div className="carousel-item">
                                <Row>
                                    <Col>
                                        <h2 className="text-xs-center component-title">{t('Awards') }</h2>
                                    </Col>
                                </Row>
                                <Row className="content">
                                    <Col lg="3" md="0"></Col>
                                    <Col lg="6" md="12">
                                        <p className="text-xs-center m-x-auto d-inline-block m-l-2 subtitle">{t('Excellent performance Dynamic Whitelist technology') }</p>
                                        <p className="text-xs-center m-x-auto d-inline-block m-b-1">{t('Again, Kaspersky Lab is placed in the \'Leaders\' section of the Gartner Magic Quadrant for Endpoint Protection Platforms.') }</p>
                                    </Col>
                                    <Col lg="3" md="0"></Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="4"></Col>
                                    <Col xs="12" md="4">
                                        <div className="text-xs-center">
                                            <img src="/images/about_us_page/gartner-logo.png" className="img-fluid"/>
                                        </div>
                                        <p className="text-xs-center">
                                            <Button color="secondary" className="text-uppercase btn-awards" onClick={() => this.handleClick('http://www.gartner.com/technology/reprints.do?id=1-26CZK42&ct=141223&st=sb')}>{t('Visit') }</Button>
                                        </p>
                                    </Col>
                                    <Col xs="12" md="4"></Col>
                                </Row>
                            </div>
                            <div className="carousel-item">
                                <Row>
                                    <Col>
                                        <h2 className="text-xs-center component-title">{t('Awards') }</h2>
                                    </Col>
                                </Row>
                                <Row className="content">
                                    <Col lg="3" md="0"></Col>
                                    <Col lg="6" md="12">
                                        <p className="text-xs-center m-x-auto d-inline-block m-l-2 subtitle">{t('Excellent performance Dynamic Whitelist technology') }</p>
                                        <p className="text-xs-center m-x-auto d-inline-block m-b-1">{t('Kaspersky Lab\'s Dynamic Whitelist technology - which helps to protect computers from malicious programs - has received the Approved Whitelisting Service certificate, following independent.') }</p>
                                    </Col>
                                    <Col lg="3" md="0"></Col>
                                </Row>
                                <Row>
                                    <Col xs="12" md="4"></Col>
                                    <Col xs="12" md="4">
                                        <div className="text-xs-center av-img-wrapper m-b-1">
                                            <img src="/images/about_us_page/AV-large-new.png" className="img-fluid av-img"/>
                                        </div>
                                        <p className="text-xs-center">
                                            <Button color="secondary" className="text-uppercase btn-awards" onClick={() => this.handleClick('http://www.kaspersky.com/about/news/product/2013/Kaspersky_Lab_Dynamic_Whitelist_technology_gets_Approved_Whitelisting_Service_certificate_from_AV_TEST?_ga=1.25792637.1119067924.1471331581')}>{t('Visit') }</Button>
                                        </p>
                                    </Col>
                                    <Col xs="12" md="4"></Col>
                                </Row>
                            </div>
                        </div>
                        <a className="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span className="icon-prev" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span className="icon-next" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </Container>
            </section>
        );
    }

}

export default Awards;
