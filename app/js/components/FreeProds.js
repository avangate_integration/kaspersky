import React, { PropTypes } from 'react';
import { Container, Row, Col, Button, ButtonDropdown } from 'reactstrap';
import {t, tct} from '../Locales';

class FreeProds extends React.Component {

  constructor(props) {
    super(props);
  }
  /**
   * all the links on this page are external ones. the below function opens them
   * in a new tab by default.
   */
  
  handleClick(link) {
    window.open(link);
  }
  render() {
    let hotLabel = <sup className="status-hot">{t('HOT') }</sup>;
    let betaLabel = <sup className="status-beta beta">{t('BETA') }</sup>;
    let newLabel = <sup className="status-new">{t('NEW') }</sup>;

    return (
      <section id="free-prods" className="free-prods">
        <Container>
          <ceva />
          <Row>
            <Col xs="12">
              <h2 className="text-xs-center component-title">{t('Select your device') }</h2>
            </Col>
          </Row>
        </Container>
        <div className="taburi">
          <div className="container-fluid">
            <ul className="nav nav-tabs row" id="myTab" role="tablist">
              <li className="nav-item col-xs-2 offset-lg-2">
                <a className="nav-link pc text-xs-center" data-toggle="tab" href="#pc" role="tab" aria-controls="pc"><span><i></i>{t('PC') }</span></a>
              </li>
              <li className="nav-item col-xs-2">
                <a className="nav-link active mac text-xs-center" data-toggle="tab" href="#mac" role="tab" aria-controls="mac"><span><i></i>{t('MAC') }</span></a>
              </li>
              <li className="nav-item col-xs-2">
                <a className="nav-link mobile text-xs-center" data-toggle="tab" href="#mobile" role="tab" aria-controls="mobile"><span><i></i>{t('Mobile') }</span></a>
              </li>
              <li className="nav-item col-xs-2">
                <a className="nav-link more text-xs-center" data-toggle="tab" href="#more" role="tab" aria-controls="more"><span><i></i>{t('More') }</span></a>
              </li>
            </ul>
          </div>
        </div>
        <div className="tab-content">
          <div className="tab-pane" id="pc" role="tabpanel">
            <div className="prod p-a-3 pc-prod">
              <Container className="">
                <Row>
                  <Col xs="12">
                    <img src="/images/free_products_page/security-scan.png"/>
                    <h6 className="product-heading">{t('Kaspersky Security Scan') }</h6>
                  </Col>
                </Row>
                <Row className="prod-text">
                  <Col xs="12" md="9">
                    <h2 className="product-desc text-uppercase">{tct('Think you\'ve got a virus? [br] Scan your PC for free!', {
                      br: <br />
                    }) }</h2>
                  </Col>
                  <Col xs="12" md="3">
                    <Button color="danger" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/acq/kss-thank-you?redef=1&THRU&reseller=gl_acq-freekasp-dl_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.102367169.1119067924.1471331581') }>{t('Start now') } <img src="/images/free_products_page/cart.png" /></Button>
                  </Col>
                </Row>
              </Container>
            </div>
            <Container>
              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/software-updater.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Software Updater') }{betaLabel}</h2>
                  <p>{t('Update your PC software & increase your security level. The tool automatically gives you a list of new updates, and applies only those ones you select.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/other/custom-html/free-tools/free-ksu-thank-you?redef=1&THRU&reseller=gl_acq-freekasp-dl_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.55097323.1119067924.1471331581') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/password-manager.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Password Manager') }{hotLabel}</h2>
                  <p>{t('Securely store your passwords & sync them across PC, Mac, iPhone & iPad and Android devices - for safer access to accounts, apps & websites. ') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://kaspersky-password-manager.en.softonic.com/download?ptn=kaspersky') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/cleaner.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky PC Cleaner') }{betaLabel}</h2>
                  <p>{t('Make your PC run smoothly & effectively, by cleaning it from junk files & temporary items - and tune up your digital privacy.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://devbuilds.kaspersky-labs.com/Fast/KCLEANER/CleanerSetup.exe') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/safe-kids.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Safe Kids') }{newLabel}</h2>
                  <p>{t('Protect your kids from digital dangers - so they can enjoy and learn more from the digital world.') }</p>
                </Col>
                <Col xs="12" md="3"  className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/safe-kids?redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.97557215.1119067924.1471331581#installation') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/security-scan.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Security Scan') }{hotLabel}</h2>
                  <p>{t('Scan your PC for viruses & malware and check it for updates. Get a detailed security status report and tips on improving your PC protection. Free.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/kss?_ga=1.97557215.1119067924.1471331581') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/rescue-disk.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Rescue Disk') }</h2>
                  <p>{t('Check and cure your computer when it is critically infected and it is impossible to boot the operating system.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://rescuedisk.kaspersky-labs.com/rescuedisk/updatable/kav_rescue_10.iso') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/virus-removal.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Virus Removal Tool') }</h2>
                  <p>{t('Get FREE & quick help for your PC - download an efficient tool to clean up your computer from viruses. Trojans, malware, adware, and more.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onclick={() => this.handleClick('http://www.kaspersky.com/advert/antivirus-removal-tool?form=1&redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.165387135.1119067924.1471331581') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/internet-security.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky internet security - multi-device') }</h2>
                  <p>{t('Get the one-license solution that delivers premium protection for your money, kids, privacy, and more on PC, Mac and Mobile.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/acq/kismd-win-thank-you?redef=1&THRU&reseller=gl_acq-freekasp-dl_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.134283185.1119067924.1471331581') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/my-kaspersky.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('My Kaspersky') }</h2>
                  <p>{t('Remotely manage the security of all your devices from a simple web portal - plus get access to free KAspersky security services.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button className="btn text-uppercase mykas" onClick={() => this.handleClick('https://center.kaspersky.com/advert/?redef=1&THRU&reseller=kpc_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.134283185.1119067924.1471331581') }>{t('Sign In') }</Button>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="tab-pane active" id="mac" role="tabpanel">
            <div className="prod p-a-3 mac-prod">
              <Container className="">
                <Row>
                  <Col xs="12">
                    <img src="/images/free_products_page/password-manager.png" className="img-fluid"/>
                    <h6 className="product-heading">{t('Kaspersky Password Manager') }</h6>
                  </Col>
                </Row>
                <Row className="prod-text">
                  <Col xs="12" md="9">
                    <h2 className="product-desc text-uppercase">{tct('Too many accounts & passwords to remember? [br] Try free password manager for mac', {
                      br: <br />
                    }) }</h2>
                  </Col>
                  <Col xs="12" md="3">
                    <Button color="danger" className="btn text-uppercase" onClick={() => this.handleClick('http://products.kaspersky-labs.com/products/multilanguage/homeuser/kpmmac/kpmmac1_0_4_489mlg_da_de_en_es_es-419_fi_fr_it_ja_ko_nb_nl_pl_pt_pt-pt_ru_sv_tr_zh-hans_zh-hant.dmg') }>{t('Get now') }<img src="/images/free_products_page/cart.png" /></Button>
                  </Col>
                </Row>
              </Container>
            </div>
            <Container>
              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/virus-scanner.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Virus Scanner') }{newLabel}</h2>
                  <p>{t('Check your Mac for viruses, Trojans, Windows & Android malware and more - and get a detailed security status report.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('https://itunes.apple.com/in/app/kaspersky-virus-scanner-free/id997030557') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/safe-kids.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Safe Kids') }{newLabel}</h2>
                  <p>{t('Protect your kids from digital dangers - so they can enjoy and learn more from the digital world.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/safe-kids?redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.123453003.1119067924.1471331581#installation') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/internet-security.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky internet security - multi-device') }</h2>
                  <p>{t('Get the one-license solution that delivers premium protection for your money, kids, privacy, and more on PC, Mac and Mobile.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/acq/kismd-mac-thank-you?redef=1&THRU&reseller=gl_acq-freekasp-dl_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.25402237.1119067924.1471331581') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/password-manager.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Password Manager') }{hotLabel}</h2>
                  <p>{t('Securely store your passwords & sync them across PC, Mac, iPhone & iPad and Android devices - for safer access to accounts, apps & websites. ') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://products.kaspersky-labs.com/products/multilanguage/homeuser/kpmmac/kpmmac1_0_4_489mlg_da_de_en_es_es-419_fi_fr_it_ja_ko_nb_nl_pl_pt_pt-pt_ru_sv_tr_zh-hans_zh-hant.dmg') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/my-kaspersky.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('My Kaspersky') }</h2>
                  <p>{t('Remotely manage the security of all your devices from a simple web portal - plus get access to free KAspersky security services.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button className="btn text-uppercase mykas" onClick={() => this.handleClick('https://center.kaspersky.com/advert/?redef=1&THRU&reseller=kpc_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.25402237.1119067924.1471331581') }>{t('Sign In') }</Button>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="tab-pane" id="mobile" role="tabpanel">
            <div className="prod p-a-3 mobile-prod">
              <Container className="">
                <Row>
                  <Col xs="12">
                    <img src="/images/free_products_page/qr.png"/>
                    <h6 className="product-heading">{t('Kaspersky QR Scanner') }</h6>
                  </Col>
                </Row>
                <Row className="prod-text">
                  <Col xs="12" md="9">
                    <h2 className="product-desc text-uppercase">{tct('QR codes are everywhere! [br] Be ready. Stay safe', {
                      br: <br />
                    }) }</h2>
                  </Col>
                  <Col xs="12" md="3">
                    <Button color="danger" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/qr-scanner?redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.92967005.1119067924.1471331581#multiplatform') }>{t('Download now') } <img src="/images/free_products_page/cart.png" /></Button>
                  </Col>
                </Row>
              </Container>
            </div>
            <Container>
              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/adcleaner.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('AdCleaner') }</h2>
                  <p>{t('Removes Blocks ads, popups, autoplay videos, tracking sccripts codes and other inappropriate cntent for clean and private mobile web browsing.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('https://app.appsflyer.com/id1053144160?pid=acq&c=acq-freekasp-COM-KAC-iOS') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/password-manager.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Password Manager') }{hotLabel}</h2>
                  <p>{t('Securely store your passwords & sync them across PC, Mac, iPhone & iPad and Android devices - for safer access to accounts, apps & websites. ') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/safe-kids.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Safe Kids') }{newLabel}</h2>
                  <p>{t('Protect your kids from digital dangers - so they can enjoy and learn more from the digital world.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/android-security.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Internet Security for Android') }</h2>
                  <p>{t('Get premium protection for your Android smartphone & tablet. Stay secure from internet threats, find your device easily if it\'s lost or stolen.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://app.appsflyer.com/com.kms.free?pid=acq&c=acq-freekasp-COM-KISA-Android') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/qr.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky QR Scanner') }{hotLabel}</h2>
                  <p>{t('Scan QR codes everwhere and see the real link before you open it. Our easy-to-use app uses the latest Kaspersky Lab technologies to keep you safe.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/qr-scanner?redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.92967005.1119067924.1471331581#multiplatform') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/safe-browser.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Safe Browser') }</h2>
                  <p>{t('Protect yourself from inappropriate content and malicious links to phishing websites that may steal your money and personal information.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/threat-scan.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Threat Scan for Android') }</h2>
                  <p>{t('Scan your phone or tablet for MasterKey, FakeID, Heartbleed & FREAK vulnerabilities to prevent your private information & bank details from being stolen.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://app.appsflyer.com/com.kaspersky.lightscanner?pid=acq&c=acq-freekasp-COM-KTSA-Android') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/my-kaspersky.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('My Kaspersky') }</h2>
                  <p>{t('Remotely manage the security of all your devices from a simple web portal - plus get access to free KAspersky security services.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button className="btn text-uppercase mykas" onClick={() => this.handleClick('https://center.kaspersky.com/advert/?redef=1&THRU&reseller=kpc_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.97704031.1119067924.1471331581') }>{t('Sign In') }</Button>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="tab-pane" id="more" role="tabpanel">
            <div className="prod p-a-3 more-prod">
              <Container className="">
                <Row>
                  <Col xs="12">
                    <img src="/images/free_products_page/internet-security.png"/>
                    <h6 className="more-product-heading">{t('Kaspersky Internet Security Multi-Device') }</h6>
                  </Col>
                </Row>
                <Row className="prod-text">
                  <Col xs="12" md="9">
                    <h2 className="more-product-desc text-uppercase">{tct('Secure your piracy, money, [br] kids & more', {
                      br: <br />
                    }) }</h2>
                  </Col>
                  <Col xs="12" md="3">
                    <Button color="danger" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/acq/multi-device?redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__ban_kl______&_ga=1.98155487.1119067924.1471331581#kismd-download') }>{t('Download now') } <img src="/images/free_products_page/cart.png" /></Button>
                  </Col>
                </Row>
              </Container>
            </div>
            <Container>
              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/kas-installer.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky Installer') }{betaLabel}</h2>
                  <p>{t('Protect all key areas of your digital life on PC, Mac, Android, iPhone and iPad.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://devbuilds.kaspersky-labs.com/Fast/smartinstaller/windows/KasperskyInstaller.exe') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/my-kaspersky.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('My Kaspersky') }</h2>
                  <p>{t('Remotely manage the security of all your devices from a simple web portal - plus get access to free KAspersky security services.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button className="btn text-uppercase mykas" onClick={() => this.handleClick('https://center.kaspersky.com/advert/?redef=1&THRU&reseller=kpc_acq-freekasp_leg_ona_dis__onl_b2c__lp-button_kl______&_ga=1.98155487.1119067924.1471331581') }>{t('Sign In') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/internet-security.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Kaspersky internet security - multi-device') }</h2>
                  <p>{t('Get the one-license solution that delivers premium protection for your money, kids, privacy, and more on PC, Mac and Mobile.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://www.kaspersky.com/advert/acq/multi-device?redef=1&THRU&reseller=gl_acq-freekasp_leg_ona_dis__onl_b2c__ban_kl______&_ga=1.98155487.1119067924.1471331581#kismd-download') }>{t('Download') }</Button>
                </Col>
              </Row>

              <Row className="vertical-align">
                <Col xs="12" md="2" lg="1">
                  <img src="/images/free_products_page/free-utilities.png" className="prodImg"/>
                </Col>
                <Col xs="12" md="7" lg="8">
                  <h2 className="component-title-left">{t('Free Utilities') }</h2>
                  <p>{t('View Kaspersky Lab\'s other free tools for removing infections from your computer.') }</p>
                </Col>
                <Col xs="12" md="3" className="buton">
                  <Button color="success" className="btn text-uppercase" onClick={() => this.handleClick('http://support.kaspersky.com/viruses/utility?CID=acq-freekasp-COM&_ga=1.97655903.1119067924.1471331581') }>{t('View all') }</Button>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </section>
    );
  }

}

export default FreeProds;
