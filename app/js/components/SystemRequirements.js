import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class SystemRequirements extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="system-requirements" className="system-requirements p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('System requirements')}</h2>
                        </Col>
                    </Row>
                     <Row>
                        <Col lg="2" md="0"></Col>
                        <Col lg="8" md="12">
                            <p className="text-xs-center m-x-auto d-inline-block m-b-3">{t('Lorem ipsum sit amet, consectetur adipiscing edit. Donec porta metus neque, sed urna maximus in morbi placerat consectetur adipiscing.')}</p>                         
                        </Col>
                        <Col lg="2" md="0"></Col>
                    </Row>
                     <Row>
                        <Col lg="4" md="0"></Col>
                        <Col lg="4" md="12">
                            <p className="text-xs-center">
                                <Button color="secondary" className="text-uppercase btn-system-requirements">{t('Read more')}</Button>
                            </p>
                        </Col>
                        <Col lg="4" md="0"></Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default SystemRequirements;