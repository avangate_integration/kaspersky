import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';
 
class UsersProtected extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="users-protected p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('400 MILLION USERS PROTECTED')}</h2>
                            <p className="text-xs-center">{t('By Kaspersky Lab Technologies')}</p>
                            <img src="/images/users-protected-icon.png" className="d-block m-x-auto m-b-2" />
                        </Col>
                    </Row>
                    <Row className="users-protected-infos">
                        <Col xs="12" md="12" lg="2" className="offset-lg-1">
                            <h3 className="text-xs-center font-weight-bold" style={{color:'#e85f7b'}}>{t('310 K')}</h3>
                            <p className="text-xs-center">{tct('NEW MALICIOUS [br] files detected by Kaspersky Lab daily', {
                                br: <br />
                            })}</p>
                        </Col>
                        <Col xs="12" md="6" lg="2">
                            <h3 className="text-xs-center font-weight-bold" style={{color:'#e9b83a'}}>{t('270 K')}</h3>
                            <p className="text-xs-center">{tct('COMPANIES [br] use Kaspersky Lab Products', {
                                br: <br />
                            })}</p>
                        </Col>
                        <Col xs="12" md="6" lg="2">
                            <h3 className="text-xs-center font-weight-bold" style={{color:'#51bba1'}}>{t('200')}</h3>
                            <p className="text-xs-center">{tct('COUNTRIES [br] and territories benefit from our products', {
                                br: <br />
                            })}</p>
                        </Col>
                        <Col xs="12" md="6" lg="2">
                            <h3 className="text-xs-center font-weight-bold" style={{color:'#c575c5'}}>{t('120')}</h3>
                            <p className="text-xs-center">{tct('GLOBAL [br] Technology/OEM Agreements', {
                                br: <br />
                            })}</p>
                        </Col>
                        <Col xs="12" md="6" lg="2">
                            <h3 className="text-xs-center font-weight-bold" style={{color:'#6da3d7'}}>{t('18')}</h3>
                            <p className="text-xs-center">{tct('YEARS [br] delivering superior security solutions', {
                                br: <br />
                            })}</p>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }

}

export default UsersProtected;
