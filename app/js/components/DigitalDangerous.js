import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';

class DigitalDangerous extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <section id="digital-dangerous" className="digital-dangerous">
                <Container className="p-a-3">
                    <Row>
                        <Col>
                            <nav className="breadcrumb">
                                <a className="m-r-1 active">{t('Home') }</a>
                                <a className="m-r-1 active">{t('Products') }</a>
                                <a>{t('Kaspersky Safe Kids') }</a>
                            </nav>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" lg="6">
                            <h1 className="digital-dangerous-title m-t-3">{tct('Digital [br] doesn\'t have [br] to be dangerous.', {
                                br: <br />
                            }) }</h1>
                            <p className="description">{t('Help your kids enjoy the digital world') }</p>
                        </Col>
                        <Col xs="12" lg="6">
                            <Container>
                                <Row>
                                    <Col xs="12" md="6" lg="5">
                                        <img src="/images/safe_kids_page/toy.png" className="toy-img"/>
                                    </Col>
                                    <Col xs="12" md="6" lg="7" className="safeKids">
                                        <div>
                                            <div className="kid-image">
                                                <img src="/images/safe_kids_page/kid.png" className="kid-img"/>
                                            </div>
                                            <div className="kid-container subtitle">
                                                <span className="">{t('Kaspersky') }</span>
                                                <span className="registered-trademark">{tct('[r] [br]', {
                                                    r: '®', br: <br />
                                                }) }</span>
                                                <span className="">{t('Safe Kids') }</span>
                                            </div>
                                        </div>
                                        <div>
                                            <Button color="danger" className="btn-digital-dangerous m-t-3 m-b-2">{t('buy now') }
                                                <img src="/images/safe_kids_page/cart-copy.png" className="cart-img"/>
                                            </Button>
                                            <p className="amount">{t('for $14.95') }</p>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default DigitalDangerous;
