import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class ExpertPick extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="expert-pick" className="expert-pick p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Expert\'s pick for online protection')}</h2>
                        </Col>
                    </Row>
                     <Row>
                        <Col lg="2" md="0"></Col>
                        <Col lg="8" md="12">
                            <p className="text-xs-center m-x-auto d-inline-block m-b-3">{t('Etiam tristique ante non lorem finibus, eu consectetur lectus vestibulum. Maecenas iaculis nisi ut est pretium congue. Cras at aliquam ipsum, vel auctor sapien. Quisque facilisis commodo hendrerit.')}</p>                         
                        </Col>
                        <Col lg="2" md="0"></Col>
                    </Row>
                    <Row>
                        <Col lg="4" md="12"></Col>
                        <Col lg="4" md="12">
                            <img src="/images/office_security_page/expert-pick.png" className="img-fluid img-full-stretch" />
                        </Col>
                        <Col lg="4" md="12"></Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default ExpertPick;