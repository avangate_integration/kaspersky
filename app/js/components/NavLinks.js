'use strict';

import React from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import {t} from '../Locales';

class NavLinks extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Nav className="pull-right-xs" id={ this.props.collapsed ? 'topmenu-collapsed' : 'topmenu'} navbar>
                <NavItem className={ this.props.collapsed ? 'hidden-md-up' : 'hidden-sm-down'}>
                    <NavLink href="/partners">{t('Partners')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? '' : 'hidden-xl-down'}>
                    <NavLink href="/about">{t('About')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? '' : 'hidden-xl-down'}>
                    <NavLink href="/compare">{t('Compare')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? '' : 'hidden-xl-down'}>
                    <NavLink href="/free-products">{t('Free Products')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? '' : 'hidden-xl-down'}>
                    <NavLink href="/office-security">{t('Office Security')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? '' : 'hidden-xl-down'}>
                    <NavLink href="/proven-leaders">{t('Proven Leaders')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? '' : 'hidden-xl-down'}>
                    <NavLink href="/safe-kids">{t('Safe Kids')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? 'hidden-md-up' : 'hidden-sm-down'}>
                    <NavLink href="#">{t('Contact')}</NavLink>
                </NavItem>
                <NavItem className={ this.props.collapsed ? 'hidden-md-up' : 'hidden-sm-down'}>
                    <NavLink href="#">{t('My Kaspersky')}</NavLink>
                </NavItem>
            </Nav>
        );
    }

}

export default NavLinks;
