import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class SmallOfficeSecurity extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="small-office-security" className="small-office-security p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Kasparsky small office security')}</h2>
                        </Col>
                    </Row>
                    <Row>                             
                        <Col lg="2" md="4">
                            <div className="text-xs-right">
                                <img src="/images/office_security_page/document1.png" />
                            </div>
                            <div className="view text-xs-right">view now</div>
                        </Col>
                        <Col lg="4" md="8">   
                            <h6 className="text-uppercase font-weight-bold text-success">{t('Kasparsky small office security')}</h6>                         
                            <p className="pages">13 PAGES</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta metus neque, sed urna maximus in morbi placerat consectetur adipiscing.</p>
                        </Col>
                        <Col lg="2" md="4">                          
                            <div className="text-xs-right">
                                <img src="/images/office_security_page/document1.png" />
                            </div>
                            <div className="view text-xs-right">view now</div>
                        </Col>
                        <Col lg="4" md="8">   
                            <h6 className="text-uppercase font-weight-bold text-success">{t('Kasparsky small office security')}</h6>                         
                            <p className="pages">13 PAGES</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta metus neque, sed urna maximus in morbi placerat consectetur adipiscing.</p>
                        </Col>
                    </Row>
                   
                </Container>
            </section>
        );
    }

}

export default SmallOfficeSecurity;