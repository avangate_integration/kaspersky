import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class Categories404 extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="categories-404" className="categories-404 p-a-3">
                <Container>
                    <Row className="m-b-3">
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="4">
                            <img src="/images/about_us_page/about_us_1.png" className="img-fluid img-full-stretch"/>
                            <h6 className="font-weight-bold text-success m-y-2">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</h6>
                            <p className="m-b-2 description">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit. Vestibulum quis.')}</p>
                        </Col>
                        <Col xs="12" md="4">
                            <img src="/images/about_us_page/about_us_2.png" className="img-fluid img-full-stretch"/>
                            <h6 className="font-weight-bold text-success m-y-2">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</h6>
                            <p className="m-b-2 description">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit. Vestibulum quis.')}</p>
                        </Col>
                        <Col xs="12" md="4">
                            <img src="/images/about_us_page/about_us_3.png" className="img-fluid img-full-stretch"/>
                            <h6 className="font-weight-bold text-success m-y-2">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</h6>
                            <p className="m-b-2 description">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit. Vestibulum quis.')}</p>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default Categories404;
