import React, { PropTypes } from 'react';
import { Card, CardText, CardBlock, CardTitle, CardSubtitle } from 'reactstrap';
import {t} from '../Locales';

class SafeKids extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <Card>
                <CardBlock>
                    <CardTitle>Card title</CardTitle>
                    <CardSubtitle>Card subtitle</CardSubtitle>
                    <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                </CardBlock>
            </Card>
        );
    }

}

export default SafeKids;
