import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class TrustExpert extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {

        return (
            <section id="trust-expert" className="trust-expert p-a-3">
                <Container>
                  <Row>
                      <Col>
                          <h2 className="text-xs-center component-title">{t('Trust the experts\' pick for online protection')}</h2>
                      </Col>
                  </Row>

                  <Row>
                    <Col xs="12" md="4">
                      <div className="top-experts"><img src="/images/proven_leader_page/expert-av.png" className="img-fluid"/></div>
                      <p className="text-xs-center">{t('"We would describe the layout of Kaspersky Internet Security\'s window as excellent. All important information and functions are displayed clearly and easy to access."')}</p>
                    </Col>
                    <Col xs="12" md="4">   
                      <div className="top-experts"><img src="/images/proven_leader_page/expert-cnet.png" className="img-fluid"/></div>                                
                      <p className="text-xs-center">{t('"Great performance, speed, and an experience appropriate for both mouse and touchescreen make Kaspersky Internet Security a top pick."')}</p>    
                    </Col>
                    <Col xs="12" md="4">
                      <div className="top-experts"><img src="/images/proven_leader_page/expert-topnet.png" className="img-fluid"/></div>
                      <p className="text-xs-center">{t('"Kaspersky continues to outperform its competitors."')}</p>                              
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="4">
                      <div className="top-experts"><img src="/images/proven_leader_page/expert-pc.png" className="img-fluid"/></div>
                      <p className="text-xs-center">{t('"It\s a new Editor\'s Choice for antivirus protection."')}</p>
                    </Col>
                    <Col xs="12" md="4">       
                      <div className="top-experts"><img src="/images/proven_leader_page/expert-gartner.png" className="img-fluid"/></div>                            
                      <p className="text-xs-center">{t('"Kaspersky Lab continues its Magic Quadrant rise. Gartner named it a \'Leader\' in endpoint protection for the fourth consecutive year."')}</p>    
                    </Col>
                    <Col xs="12" md="4">
                      <div className="top-experts"><img src="/images/proven_leader_page/expert-pcworld.png" className="img-fluid"/></div>
                      <p className="text-xs-center">{t('"Its exceptional all-around protection, extremely user-friendly interface, and small footprint make Kaspersky perfect for novices and advanced users alike."')}</p>                              
                    </Col>
                  </Row>
                </Container>
            </section>
        );
    }

}

export default TrustExpert;
