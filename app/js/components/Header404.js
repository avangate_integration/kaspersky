import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class Header404 extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {

        return (
            <section id="header-404" className="header-404 p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <nav className="breadcrumb">
                                <a className="m-r-1 active">{t('Home')}</a>
                                <a>{t('404')}</a>
                            </nav>
                        </Col>
                    </Row>
                    <Row className="m-t-2">
                        <Col>
                            <h2 className="title-404">{t('Kaspersky Lab | 404')}</h2>
                        </Col>
                    </Row>
                    <Row className="border-left">
                        <div className="subtitle">{t('Oops!')}</div>
                        <div className="description">{t('Page you are looking for doesn\'t exist.')}</div>
                    </Row>
                    <Row className="m-t-2 long-description">
                        <Col>
                            <span className="homepage-redirect">{t('Go back to homepage ')}</span>
                            <span>{t('or check the list below.')}</span>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default Header404;
