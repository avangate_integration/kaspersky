import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class HelpMeChoose extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="help-me-choose" className="help-me-choose p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Not sure which product is right for you?')}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="4" md="0" className="help-me-choose-img">
                            <img src="/images/home_page/help-me-choose.png" className="img-fluid d-block m-x-auto pull-lg-right" />
                        </Col>
                        <Col lg="4" md="12" className="help-me-choose-infos">
                            
                           
                             <p className="text-xs-center subtitle">{t('Find the right product in just 30 seconds!')}</p>
                             <p className="text-xs-center">
                                <Button color="secondary" className="text-uppercase btn-dark btn-choose">{t('Help me choose')}</Button>
                             </p>
                        </Col>
                        <Col lg="4" md="0"></Col>
                    </Row>
                </Container>
              </section>
        );
    }

}

export default HelpMeChoose;
