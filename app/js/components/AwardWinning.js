import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';

class AwardWinning extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="award-win" className="award-win p-a-3">
              <Container>
                  <Row>
                    <Col>
                        <h2 className="text-xs-center component-title">{t('Our award-winning security products')}</h2>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="12" lg="6">
                      <div>
                        <img src="/images/proven_leader_page/internet-security.png" className="img-fluid img-security"/>
                        <div className="security">
                          <h6 className="text-uppercase subtitle m-t-2 m-b-2">{t('Security for home')}</h6>
                          <p className="kasgreen kasgreen-subtitle">{t('Kaspersky')}</p>
                          <h4>{tct('Internet [br] Security', {
                              br: <br />
                          })}</h4>
                          <p>{t('multi-device 2016')}</p>
                        </div>
                      </div>
                      <div>
                        <div className="layer text-xs-center m-b-1"><img src="/images/proven_leader_page/suitcase.png" className="img-fluid"/></div>
                        <p className="security-text">{t('Adds extra layers of security for online banking & shopping')}</p>
                      </div>
                      <div>
                        <div className="layer text-xs-center"><img src="/images/proven_leader_page/kids.png" className="img-fluid"/></div>
                        <p className="security-text">{t('Helps you keep your kids safe from Internet dangers & more')}</p>
                      </div>
                    </Col>
                    <Col xs="12" md="12" lg="6" className="security-for-small-office">
                      <div>
                        <img src="/images/proven_leader_page/small-office-security.png" className="img-fluid img-security m-t-2"/>
                        <div className="security">
                          <h6 className="text-uppercase subtitle m-t-2 m-b-2">{t('Security for small office')}</h6>
                          <p className="kasgreen kasgreen-subtitle-office">{t('Kaspersky')}</p>
                          <h4>{tct('Small Office [br] Security', {
                              br: <br />
                          })}</h4>
                        </div>
                      </div>
                      <div className="small-security m-t-1">
                        <div className="layer text-xs-center m-b-1"><img src="/images/proven_leader_page/laptops.png" className="img-fluid"/></div>
                        <p className="security-text">{t('Defend laptops, desktops & servers against malware')}</p>
                      </div>
                      <div>
                        <div className="layer text-xs-center"><img src="/images/proven_leader_page/banking.png" className="img-fluid"/></div>
                        <p className="security-text">{t('Add extra security for online banking and payments')}</p>
                      </div>
                    </Col>
                  </Row>
               </Container>     
            </section>
        );
    }

}

export default AwardWinning;
