import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class SafeKids extends React.Component {

    constructor(props) {
        super(props);
    }
    handleClick(link) {
        window.open(link, '_self');
    }
    render() {
        return (
            <section id="safe-kids" className="banner lisible-text-banner safe-kids p-a-3">
                <Container>
                    <Row>
                        <Col lg="2" md="0"></Col>
                        <Col lg="8" md="12" className="text-xs-center">
                            <h2 className="text-xs-center component-title m-b-2">{t('Digital doesn\'t have to be dangerous') }</h2>
                            <div className="lisible-text">
                                <p className="text-xs-center m-x-auto d-inline-block m-b-3 safe-subtitle">{t('Help your kids enjoy the digital world.') }</p>
                            </div>
                            <div className="text-xs-center m-t-1">
                                <Button color="secondary" className="text-uppercase btn-red btn-safe-kids" onClick={() => this.handleClick('http://localhost:3000/safe-kids') }>{t('Try Now') }</Button>
                            </div>
                        </Col>
                        <Col xs="12" md="6" lg="2" md="0" className="text-xs-center">
                            <p className="text-xs-center m-t-1">
                                <img src="/images/home_page/free_tools.png" className="img-fluid m-x-auto d-inline-block"/>
                            </p>
                            <div className="tools-1">{t('Free Tools') }</div>
                            <div className="tools-2 lisible-text"><p>{t('Boost your security for free') }</p></div>
                            <a href="/free-products" className="safe-link">Try our free products</a>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default SafeKids;
