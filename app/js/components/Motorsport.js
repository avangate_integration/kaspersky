import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class Motorsport extends React.Component {

    constructor(props) {
        super(props);
    }
    handleClick(link) {
        window.open(link);
    }
    render() {

        return (
            <section id="motorsport" className="motorsport p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title-white">{t('Kaspersky Motorsport') }</h2>
                        </Col>
                    </Row>

                    <Row>
                        <Col lg="3" md="0"></Col>
                        <Col lg="6" md="12">
                            <p className="text-xs-center m-x-auto d-inline-block m-b-3">{t('From team sponsorship and supporting young drivers our close relationship with Scuderia Ferrari extends far beyond the track.') }</p>
                        </Col>
                        <Col lg="3" md="0"></Col>
                    </Row>

                    <Row>

                        <Col xs="12" md="4"></Col>

                        <Col xs="12" md="4">
                            <p className="text-xs-center">
                                <Button color="success" className="text-uppercase btn" onClick={() => this.handleClick('http://kasperskymotorsport.com') }>{t('MORE') }</Button>
                            </p>

                        </Col>

                        <Col xs="12" md="4"></Col>

                    </Row>

                </Container>
            </section >
        );
    }

}

export default Motorsport;
