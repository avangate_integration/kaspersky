import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class BodyColumns404 extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {

        return (
            <section id="body-columns-404" className="body-columns-404 p-a-3">
              <Container>
                 <Row className="m-b-3">
                      <Col>
                          <h2 className="text-xs-center component-title">{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</h2>
                      </Col>
                  </Row>
                  <Row className="body-columns-content">
                    <Col lg="3" className="home-users">
                        <h6 className="title-body-columns-404"> {t('Home users')} </h6>
                        <p>
                            <ul className="ul-bullet">
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                            </ul>
                        </p>
                    </Col>
                    <Col lg="3" className="small-business">
                        <h6 className="title-body-columns-404"> {t('Small business')} </h6>
                        <p>
                            <ul className="ul-bullet">
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                            </ul>
                        </p>
                    </Col>
                    <Col lg="3" className="medium-business">
                        <h6 className="title-body-columns-404"> {t('Medium business')} </h6>
                        <p>
                            <ul className="ul-bullet">
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                            </ul>
                        </p>
                    </Col>
                    <Col lg="3" className="enterprise">
                        <h6 className="title-body-columns-404"> {t('Enterprise')} </h6>
                        <p>
                            <ul className="ul-bullet">
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                                <li>{t('Lorem ipsum dolot sit amet, consectetur adipiscing elit')}</li>
                            </ul>
                        </p>
                    </Col>
                  </Row>
               </Container>
            </section>
        );
    }

}

export default BodyColumns404;
