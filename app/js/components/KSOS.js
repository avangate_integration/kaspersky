import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t, tct} from '../Locales';
import AvaAPI   from '../utils/AvaAPI';
import Loading from '../utils/Loading';
import Helpers from '../utils/Helpers';

class KSOS extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }

  componentDidMount() {
    let self = this;
    AvaAPI.getProducts({
      PRODS: 'KL4867S',
      LANG: 'EN',
      CURRENCY: 'EUR'
    }).then(res => {
      self.setState({ products: JSON.parse(res.text).data });
    });
    // let shortDescription = prod.shortDescription;
  }

  render() {
    return (
      <Loading show={this.state.products.length == 0} width="200" height="100">
        {this.state.products.map((prod, index) => {
          return (
            <section id="ksos" className="ksos p-a-3" key={prod.code}>
              <Container>
                <Row>
                  <Col lg="3">
                    <img src={prod.image} className="compare-products-item-img" height="300"  />
                  </Col>
                  <Col lg="6">
                    <h2>{prod.name}</h2>
                    <div dangerouslySetInnerHTML={{ __html: prod.shortDescription }} />

                  </Col>
                  <Col lg="3" className="price-container">
                    <h2>{prod.price.net}</h2>
                  </Col>
                </Row>
              </Container>
            </section>
          )
        }) }
      </Loading>
    );
  }

}

export default KSOS;
