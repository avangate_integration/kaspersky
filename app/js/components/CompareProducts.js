'use strict';

import React from 'react';
import AvaAPI   from '../utils/AvaAPI';
import { Container, Row, Col, Button } from 'reactstrap';
import Loading from '../utils/Loading';
import {t, tct} from '../Locales';
import Responsive from '../utils/Responsive';
import ProductPrice from './ProductPrice';
// import ProductsActions from '../actions/ProductsActions';
// import ProductsStore   from '../stores/ProductsStore';

/**
 * 
 * 
 * @class CompareProducts
 * @extends {React.Component}
 */
class CompareProducts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
        this.productsOrder = ['7CBC44F3B4','4605281','214865B32E'];
        this.productsData = this.reorderProducts(this.props.productsData, this.productsOrder);
        this.productFeatures = this.props.productFeatures;
    }

    makeLayoutResponsive() {
        Responsive.equalizeHeight('.compare-products-item-header');
        Responsive.equalizeHeight('.product-feature-name, .product-feature-checkbox');
        Responsive.equalizeHeight('.product-price');
    }
    
    componentDidMount() {
        let self = this;
        let options = {
            PRODS: '7CBC44F3B4,4605281,214865B32E',
            LANG: 'EN',
            CURRENCY: 'EUR'
        };
        AvaAPI.getProducts(options).then(res => {
            self.setState({ products: self.reorderProducts(JSON.parse(res.text).data, self.productsOrder) });
        });
        this.makeLayoutResponsive();
        window.addEventListener('resize', this.makeLayoutResponsive);
    }

    componentDidUpdate() {
        this.makeLayoutResponsive();
    }

    reorderProducts(products, order) {
        let orderedProducts = [];
        for (let i=0; i<order.length; i++) {
            for (let j=0; j<products.length; j++) {
                if (products[j]['code'] === order[i]) {
                    orderedProducts.push(products[j]);
                }
            }
        }
        return orderedProducts;
    }

    getProductData(productsData, productCode) {
        for (let i=0; i<productsData.length; i++) {
            if (productsData[i]['code'] === productCode) {
                return productsData[i];
            }
        }
    }

    handleCurrencyChange() {

    }

    handleDevicesChange() {
        
    }

    handleYearsChange() {
        
    }

    render() {
        console.log(this.state.products);
        return (
            <Loading show={this.state.products.length == 0} width="200" height="100">
                <section id="compare-products" className="compare-products m-b-3">
                    <Container>
                        <Row className="m-t-2 m-b-2">
                            <Col>
                                <h2 className="text-xs-center component-title">{t('Products for Home') }</h2>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="3" className="hidden-md-down"></Col>
                            <Col xs="12" lg="9">
                                <Row>
                                    {this.productsData.map((productData, index) => {
                                        return (
                                            <Col xs="4" key={productData.code} className={productData.recommended ? 'recommended-product' : ''}>
                                                {productData.recommended ? <div className="recommended-tooltip">{t('Recommended')}</div> : ' ' }
                                            </Col>
                                        )
                                    }) }
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="3" className="compare-products-side-left p-y-3 hidden-md-down">
                                <span className="text-xs-center d-block">{tct('Scroll down [br] to see [br] comparison chart', {
                                    br: <br />
                                }) }</span><br />
                                <img src="/images/home_page/scroll_down_icon.png" className="d-block m-x-auto" />
                            </Col>
                            <Col xs="12" lg="9">
                                <Row>
                                    {this.state.products.map((prod, index) => {
                                        let productData = this.getProductData(this.productsData, prod.code);
                                        console.log(productData);
                                        return (
                                            <Col xs="4" key={prod.code}  className={'compare-products-item ' + (productData.recommended ? 'recommended-product' : '')}>
                                                <div className="compare-products-item-header p-t-2">
                                                    {productData.promo ? <div className="promo-badge"><span className="promo-badge-text">{productData.promo}</span></div> : ''}
                                                    <img src={prod.image} className="compare-products-item-img" width="60" />
                                                    <h3 className="compare-products-item-name m-b-0">{prod.name}</h3>
                                                </div>
                                            </Col>
                                        )
                                    }) }
                                </Row>
                                <Row>
                                    {this.state.products.map((prod, index) => {
                                        let productData = this.getProductData(this.productsData, prod.code);
                                        return (
                                            <Col xs="4" key={prod.code}  className={'compare-products-item ' + (productData.recommended ? 'recommended-product' : '')}>
                                                <a href="#" className="color-inherit text-uppercase btn btn-secondary d-block m-b-1 compare-products-item-sys-req-btn">{t('System Requirements')} <i className="fa fa-chevron-right icon" aria-hidden="true"></i></a>
                                                <button className="text-uppercase font-weight-bold btn btn-red btn-secondary m-b-1 d-block compare-products-item-buy-now-bn ">{t('Buy Now')} <i className="fa fa-shopping-cart fa-flip-horizontal" aria-hidden="true"></i></button>
                                                <a href={prod.trialUrl} className="text-uppercase btn btn-dark btn-secondary m-b-1  d-block compare-products-item-free-trial-btn">{t('Free Trial')}</a>
                                            </Col>
                                        )
                                    }) }
                                </Row>
                            </Col>
                        </Row>
                        {this.productFeatures.map((productFeature, index) => {
                            let currentFeature = productFeature.feature;
                            return (
                                <Row key={'product_feature_' + index} className={'product-feature ' + currentFeature + ' ' + ((index + 1) % 2 === 0 ? 'even' : 'odd')}>
                                    <Col lg="3" xs="12">
                                        <div className="product-feature-name text-xs-center text-lg-left">
                                            <div className="product-feature-title">{productFeature.title}</div>
                                            <div className="product-feature-description">{productFeature.description}</div>
                                        </div>
                                    </Col>
                                    <Col xs="12" lg="9">
                                        <Row>
                                            {this.productsData.map((productData, index) => {
                                                return (
                                                    <Col xs="4" key={productData.code} className={productData.recommended ? 'recommended-product' : ''}>
                                                        <div className={(this.productsData.length - 1) === index ? 'product-feature-checkbox text-xs-center last' : 'product-feature-checkbox text-xs-center'}>
                                                            {productData.features[currentFeature] ? <i className="fa fa-check product-feature-check" aria-hidden="true"></i> : ' ' }
                                                        </div>
                                                    </Col>
                                                )
                                            }) }
                                        </Row>
                                    </Col>
                                </Row>
                            )
                        }) }
                        <Row>
                            <Col lg="3" className="hidden-md-down">
                            </Col>
                            <Col xs="12" lg="9">
                                <Row>
                                    {this.state.products.map((prod, index) => {
                                        let productData = this.getProductData(this.productsData, prod.code);
                                        return (
                                            <Col xs="4" key={prod.code}  className={'compare-products-item ' + (productData.recommended ? 'recommended-product' : '')}>
                                                <button className="text-uppercase font-weight-bold btn btn-red btn-secondary m-t-1 m-b-1 d-block compare-products-item-buy-now-bn ">{t('Buy Now')} <i className="fa fa-shopping-cart fa-flip-horizontal" aria-hidden="true"></i></button>
                                                <ProductPrice productData={prod} className="m-b-1 text-xs-center" />
                                                <select onChange={this.handleCurrencyChange} className="selectpicker hidden-xs-up">
                                                    <option>Mustard</option>
                                                    <option>Ketchup</option>
                                                    <option>Relish</option>
                                                </select>
                                                <select onChange={this.handleDevicesChange} className="selectpicker hidden-xs-up">
                                                    <option>Mustard</option>
                                                    <option>Ketchup</option>
                                                    <option>Relish</option>
                                                </select>
                                                <select onChange={this.handleYearsChange} className="selectpicker hidden-xs-up">
                                                    <option>Mustard</option>
                                                    <option>Ketchup</option>
                                                    <option>Relish</option>
                                                </select>
                                                <a href="#" className="color-inherit text-uppercase btn btn-secondary d-block m-b-1 compare-products-item-sys-req-btn">{t('US DOLLARS')} <i className="fa fa-chevron-right icon" aria-hidden="true"></i></a>
                                                <a href="#" className="color-inherit text-uppercase btn btn-secondary d-block m-b-1 compare-products-item-sys-req-btn">{t('Number of Devices')} <i className="fa fa-chevron-right icon" aria-hidden="true"></i></a>
                                                <a href="#" className="color-inherit text-uppercase btn btn-secondary d-block m-b-1 compare-products-item-sys-req-btn">{t('Years of Protection')} <i className="fa fa-chevron-right icon" aria-hidden="true"></i></a>
                                                <a href={prod.trialUrl} className="text-uppercase btn btn-dark btn-secondary m-b-1  d-block compare-products-item-free-trial-btn">{t('Free Trial')}</a>
                                            </Col>
                                        )
                                    }) }
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Loading>
        );
    }
}

export default CompareProducts;