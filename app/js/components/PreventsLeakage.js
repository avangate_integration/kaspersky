import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class PreventsLeakage extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="prevents-leakage" className="prevents-leakage p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Prevents leakage of company & customer information')}</h2>
                        </Col>
                    </Row>
                    <Row className="m-t-2">
                        <Col lg="6" md="12" className="prevents-leakage-img">
                            <img src="/images/office_security_page/protects-android-devices.png" className="img-fluid img-full-stretch"/>
                        </Col>
                        <Col lg="6" md="12">
                            <p className="m-b-2 prevents-leakage-infos">{t('Remote access to system and information is a vital part of everyday business. However, mobile malware attacks - and the loss or theft of phones withbusiness data - can cause serious issues. That\'s why our best-of-breed protection for Android phones and tablets:')}</p>
                            <p>
                              <ul className="ul-bullet">
                                <li>{t('Defends against malware and cybercriminals')}</li>
                                <li>{t('Blocks unwanted calls and texts')}</li>
                                <li>{t('Protects confidential information on lost or stolen phones')}</li>
                              </ul>
                            </p>
                            <p className="m-t-3 btn-prevents-leakage-wrapper">
                                <Button color="secondary" className="text-uppercase">{t('Read more')}</Button>
                            </p>
                        </Col>
                    </Row>
                 </Container>
            </section>
        );
    }

}

export default PreventsLeakage;
