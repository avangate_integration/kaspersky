'use strict';

import React from 'react';

class LanguageSelector extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <div className="language-selector dropdown">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="fa fa-globe globe-icon" aria-hidden="true"></i>
                    <i className="fa fa-chevron-down chevron-icon" aria-hidden="true"></i> 
                    <span className="btn-text">United States</span>
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="#">Romania</a>
                </div>
            </div>
        );
    }

}

export default LanguageSelector;