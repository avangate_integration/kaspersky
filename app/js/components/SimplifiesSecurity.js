import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class SimplifiesSecurity extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="simplifies-security" className="simplifies-security p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Simplifies Security - Including remote management')}</h2>
                        </Col>
                    </Row>
                    <Row className="m-t-2 simplifies-security-img">
                        <Col lg="6" md="12">
                            <img src="/images/office_security_page/simplifies-security.png" className="img-fluid img-full-stretch"/>
                        </Col>
                        <Col lg="6" md="12" className="simplifies-security-infos">
                            <p className="m-b-2">{t('We\'ve designed Kasperky Small Office Security to be extremely easy to use - so managing your IT security isn\'t a chore and you can spend more time on your core business activities:')}</p>
                            <p>
                              <ul className="ul-bullet">
                                <li>{t('Enables centralized & remote management of your IT security')}</li>
                                <li>{t('Lets you control use of the Web, social networking & more')}</li>
                                <li>{t('Helps keep PCs \'clean\' - with File Shredder & PC Cleaner')}</li>
                              </ul>
                            </p>
                            <p className="m-t-3 btn-simplifies-security-wrapper">
                                <Button color="secondary" className="text-uppercase">{t('Read more')}</Button>
                            </p>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default SimplifiesSecurity;
