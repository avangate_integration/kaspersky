import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class WorldClass extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {

        return (
            <section id="world-class" className="world-class p-a-3">
                <Container>
                    <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <div className="carousel-inner" role="listbox">
                            <div className="carousel-item active">
                              <Row>
                                  <Col>
                                      <h2 className="text-xs-center component-title">{t('World-class protection that makes headlines.')}</h2>
                                  </Col>
                              </Row>

                              <Row>
                                <Col xs="12" md="4">
                                  <div className="text-block">{t('"Arguably the most important Internet security company in the world."')}</div>
                                  <h6 className="headline">{t('Wired')}</h6>
                                </Col>
                                <Col xs="12" md="4">                                   
                                  <div className="text-block">{t('"A solid product - just what we would expect from these venerable pros."')}</div>
                                  <h6 className="headline">{t('SC Magazine')}</h6>                                   
                                </Col>
                                <Col xs="12" md="4">
                                  <div className="text-block">{t('"Kaspesky Lab has repeatedly impressed skeptics by exposing genuine and serious cyber-security problems."')}</div>
                                  <h6 className="headline">{t('The Economist')}</h6>                                      
                                </Col>
                              </Row>
                            </div>

                            <div className="carousel-item">
                                <Row>
                                    <Col>
                                        <h2 className="text-xs-center component-title">{t('World-class protection that makes headlines.')}</h2>
                                    </Col>
                                </Row>

                                <Row>
                                  <Col xs="12" md="4">
                                    <div className="text-block">{t('"Arguably the most important Internet security company in the world."')}</div>
                                    <h6 className="headline">{t('Wired')}</h6>
                                  </Col>
                                  <Col xs="12" md="4">                                   
                                    <div className="text-block">{t('"A solid product - just what we would expect from these venerable pros."')}</div>
                                    <h6 className="headline">{t('SC Magazine')}</h6>                                   
                                  </Col>
                                  <Col xs="12" md="4">
                                    <div className="text-block">{t('"Kaspesky Lab has repeatedly impressed skeptics by exposing genuine and serious cyber-security problems."')}</div>
                                    <h6 className="headline">{t('The Economist')}</h6>                                      
                                  </Col>
                                </Row>
                            </div>

                            <div className="carousel-item">
                                <Row>
                                    <Col>
                                        <h2 className="text-xs-center component-title">{t('World-class protection that makes headlines.')}</h2>
                                    </Col>
                                </Row>

                                <Row>
                                  <Col xs="12" md="4">
                                    <div className="text-block">{t('"Arguably the most important Internet security company in the world."')}</div>
                                    <h6 className="headline">{t('Wired')}</h6>
                                  </Col>
                                  <Col xs="12" md="4">                                   
                                    <div className="text-block">{t('"A solid product - just what we would expect from these venerable pros."')}</div>
                                    <h6 className="headline">{t('SC Magazine')}</h6>                                   
                                  </Col>
                                  <Col xs="12" md="4">
                                    <div className="text-block">{t('"Kaspesky Lab has repeatedly impressed skeptics by exposing genuine and serious cyber-security problems."')}</div>
                                    <h6 className="headline">{t('The Economist')}</h6>                                      
                                  </Col>
                                </Row>
                            </div>
                        </div>
                    </div>
                
                </Container>
            </section>
        );
    }

}

export default WorldClass;
