import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class ProductImage extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (
            <section id="product-image" className="product-image p-a-3">
                <Container>
                    <Row>
                        <Col lg="6" md="12" className="m-t-3">
                            <p>{t('Kaspersky')}</p>
                            <h1 className="title-product-image">{t('Internet Security')}</h1>
                            <p className="m-b-3">{t('multi-device')}</p>
                            <Button color="danger" className="btn-product-image m-t-3">{t('Buy Now')}</Button>
                        </Col>
                         <Col lg="6" md="12"></Col>
                    </Row>
                 </Container>
            </section>
        );
    }

}

export default ProductImage;
