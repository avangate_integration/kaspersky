import React, { PropTypes } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {t} from '../Locales';

class RenewLicense extends React.Component {

  constructor(props) {
    super(props);
  }
    render() {
        return (

            <section className="renew-license p-a-3">
                <Container>
                    <Row>
                        <Col>
                            <h2 className="text-xs-center component-title">{t('Renew your license')}</h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="3" md="0"></Col>
                        <Col lg="6" md="12" className="text-xs-center">
                            <p className="m-x-auto d-inline-block m-b-3 m-l-3 renew-subtitle">{t('Renew your full-year licence and get 30% discounts!')}</p>
                            <p>
                                <Button color="secondary" className="text-uppercase renew-btn">{t('Renew License')}</Button>
                            </p>
                            <p className="note">{t('NOTE: only users with valid full-year licenses can renew or claim discounts.')}</p>
                        </Col>
                        <Col lg="3" md="0"></Col>
                    </Row>
                </Container>
            </section>
        );
    }

}

export default RenewLicense;
