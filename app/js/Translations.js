/**
 * zh-cn => zh_CN
 * 
 * @param {any} language
 * @returns
 */
function convertLocaleFormat(language) {
    let [left, right] = language.split('-');
    return left + (
        right ? '_' + right.toUpperCase() : ''
    );
}

/**
 * 
 * 
 * @export
 * @param {any} language
 * @returns
 */
export function getTranslations(language) {
    language = convertLocaleFormat(language);
    // let translationsPath = '../../build/translations/' + language + '.json';
    return require('../../build/public/translations/en.json');
}

/**
 * 
 * 
 * @export
 * @param {any} language
 * @returns
 */
export function translationsExist(language) {
    language = convertLocaleFormat(language);
    try {
        require('../../build/public/translations/' + language + '.json');
    } catch (e) {
        return false;
    }
    return true;
}