
'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import BecomeAPartner      from '../components/BecomeAPartner';

const propTypes = {
    currentUser: React.PropTypes.object
};

class PartnersPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="PartnersPage">
                <section className="home-page">
                    <BecomeAPartner />
                    <img src="/images/temp/Partners.jpg" alt="about" />

                </section>
            </DocumentTitle>
        );
    }

}

PartnersPage.propTypes = propTypes;

export default PartnersPage;
