'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import UsersProtected from '../components/UsersProtected';
import FreeProds from '../components/FreeProds';

const propTypes = {
    currentUser: React.PropTypes.object
};

class FreeProductsPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="Free Products">
                <section className="free-prods">
                    <FreeProds />
                    <UsersProtected />
                    <img src="/images/temp/Free_products.jpg" alt="about" />

                </section>
            </DocumentTitle>
        );
    }

}

FreeProductsPage.propTypes = propTypes;

export default FreeProductsPage;
