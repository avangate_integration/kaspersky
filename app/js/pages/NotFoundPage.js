'use strict';

import React         from 'react';
import DocumentTitle from 'react-document-title';
import Header404 from '../components/Header404.js';
/*import BodyColumns404 from '../components/BodyColumns404.js';
import Categories404 from '../components/Categories404.js';*/

const propTypes = {
    currentUser: React.PropTypes.object
};

class NotFoundPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="Kaspersky Lab| Antivirus Protection & Internet Security software">
                <section className="not-found-page">              
                    <Header404 />
                </section>
            </DocumentTitle>
        );
    }

}

NotFoundPage.propTypes = propTypes;

export default NotFoundPage;
