'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import Company from '../components/Company.js';
import ManagementTeam from '../components/ManagementTeam.js';
import Awards from '../components/Awards.js';
import FollowUs from '../components/FollowUs.js';
import SaveTheWorld from '../components/SaveTheWorld.js';

const propTypes = {
    currentUser: React.PropTypes.object
};

class AboutPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="About">
                <section className="home-page">
                    
                    <SaveTheWorld />
                    <Company />
                    <Awards />
                    <ManagementTeam />
                    <FollowUs />
                    <UsersProtected />
                </section>
            </DocumentTitle>
        );
    }

}

AboutPage.propTypes = propTypes;

export default AboutPage;
