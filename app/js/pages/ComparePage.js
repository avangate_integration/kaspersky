'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';

const propTypes = {
    currentUser: React.PropTypes.object
};

class ComparePage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="Compare">
                <section className="home-page">

                    <img src="/images/temp/compare_products.jpg" alt="about" />

                </section>
            </DocumentTitle>
        );
    }

}

ComparePage.propTypes = propTypes;

export default ComparePage;
