
'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import DefendsMalware from '../components/DefendsMalware';
import SimplifiesSecurity from '../components/SimplifiesSecurity';
import AddExtraSecurity from '../components/AddExtraSecurity';
import ProtectsAndroidDevices from '../components/ProtectsAndroidDevices';
import PhishingInternetRisks from '../components/PhishingInternetRisks';
import PreventsLeakage from '../components/PreventsLeakage';
import SystemRequirements from '../components/SystemRequirements';
import ExpertPick from '../components/ExpertPick';
import SmallOfficeSecurity from '../components/SmallOfficeSecurity';
import KSOS from '../components/KSOS';

const propTypes = {
    currentUser: React.PropTypes.object
};

class OfficeSecurityPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
                   // <DefendsMalware />
                    // <SimplifiesSecurity />
                    // <AddExtraSecurity />
                    // <ProtectsAndroidDevices />
                    // <PhishingInternetRisks />
                    // <PreventsLeakage />
                    // <SystemRequirements />
                    // <ExpertPick />
                    // <SmallOfficeSecurity />
        return (
            <DocumentTitle title="Office Security">
                <section className="home-page">
                    <KSOS />
         
                    <img src="/images/temp/Office_security.jpg" alt="about" />
                </section>
            </DocumentTitle>
        );
    }

}

OfficeSecurityPage.propTypes = propTypes;

export default OfficeSecurityPage;
