
'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import ProvenLeader  from '../components/ProvenLeader';
import LeaderGraph  from '../components/LeaderGraph';
import WorldClass  from '../components/WorldClass';
import TrustExpert  from '../components/TrustExpert';
import UsersProtected  from '../components/UsersProtected';
import AwardWinning  from '../components/AwardWinning';
import SecurityScan  from '../components/SecurityScan';

const propTypes = {
  currentUser: React.PropTypes.object
};

class ProvenLeadersPage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <DocumentTitle title="ProvenLeadersPage">
        <section className="proven-leader">
          <ProvenLeader />
          <LeaderGraph />
          <TrustExpert />
          <AwardWinning />
          <UsersProtected />
          <img src="/images/temp/Proven_Leader.jpg" alt="about" />

        </section>
      </DocumentTitle>
    );
  }

}

ProvenLeadersPage.propTypes = propTypes;

export default ProvenLeadersPage;
