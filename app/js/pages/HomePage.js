'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import UsersProtected from '../components/UsersProtected';
import HelpMeChoose from '../components/HelpMeChoose';
import RenewLicense from '../components/RenewLicense';
import CompareProducts from '../components/CompareProducts';
import BusinessSecurity from '../components/BusinessSecurity';
import AboutUs from '../components/AboutUs';
import WhyUs from '../components/WhyUs';
import SafeKids from '../components/SafeKids';
import Motorsport from '../components/Motorsport';
import ProductImage from '../components/ProductImage';

import productsData from '../data/HomepageProducts';
import productFeatures from '../data/ProductFeatures';

const propTypes = {
    currentUser: React.PropTypes.object
};

class HomePage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="Home">
                <div id="home-page">
                    <ProductImage />
                    <CompareProducts productsData={productsData} productFeatures={productFeatures} />
                    <HelpMeChoose />
                    <RenewLicense />
                    <BusinessSecurity />
                    <SafeKids />
                    <AboutUs />
                    <WhyUs />
                    <Motorsport />
                    <UsersProtected />
                </div>
            </DocumentTitle>
        );
    }

}

HomePage.propTypes = propTypes;

export default HomePage;
