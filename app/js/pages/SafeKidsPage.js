
'use strict';

import React         from 'react';
import {Link}        from 'react-router';
import DocumentTitle from 'react-document-title';
import DigitalDangerous from '../components/DigitalDangerous.js';
import SafeKidsBodyComponents from '../components/SafeKidsBodyComponents.js';
import UsersProtected from '../components/UsersProtected.js';
import MoreSolutions from '../components/MoreSolutions.js';

const propTypes = {
    currentUser: React.PropTypes.object
};

class SafeKidsPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DocumentTitle title="Safe Kids">
                <section className="home-page">
                    <DigitalDangerous />
                    <SafeKidsBodyComponents />
                    <MoreSolutions />
                    <UsersProtected />
                    
                    <img src="/images/temp/Safe_Kids.jpg" alt="about" />

                </section>
            </DocumentTitle>
        );
    }

}

SafeKidsPage.propTypes = propTypes;

export default SafeKidsPage;
