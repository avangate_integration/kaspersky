'use strict';

import React                       from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import CreateBrowserHistory        from 'history/lib/createBrowserHistory';

import App                         from './App';
import HomePage                    from './pages/HomePage';
import SearchPage                  from './pages/SearchPage';
import NotFoundPage                from './pages/NotFoundPage';

import AboutPage				   from './pages/AboutPage';
import ComparePage				   from './pages/ComparePage';
import FreeProductsPage			   from './pages/FreeProductsPage';
import OfficeSecurityPage		   from './pages/OfficeSecurityPage';
import PartnersPage			       from './pages/PartnersPage';
import ProvenLeadersPage           from './pages/ProvenLeadersPage';
import SafeKidsPage                from './pages/SafeKidsPage';

export default (
    <Router history={CreateBrowserHistory() }>
        <Route path="/" component={App}>
            <IndexRoute component={HomePage} />

            <Route path="/" component={HomePage} />
            <Route path="/search" component={SearchPage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/compare" component={ComparePage} />
            <Route path="/free-products" component={FreeProductsPage} />
            <Route path="/office-security" component={OfficeSecurityPage} />
            <Route path="/partners" component={PartnersPage} />
            <Route path="/proven-leaders" component={ProvenLeadersPage} />
            <Route path="/safe-kids" component={SafeKidsPage} />

            <Route path="*" component={NotFoundPage} />
        </Route>
    </Router>
);
